--TABLE of Purchase Types
CREATE TABLE "PurchaseTypes"
("PurchaseTypeID" number,
"Name" varchar(10),
"Description" varchar(250),
"ResourceCode" varchar(250) null,
"Rank" number null,
"ApplicationID" number null)
;

INSERT INTO "PurchaseTypes" VALUES(1, 'LI', 'Life Insurance', NULL, 1, NULL );

INSERT INTO "PurchaseTypes" VALUES(2, 'IR', 'Investments Regular', NULL, 2, NULL );

INSERT INTO "PurchaseTypes" VALUES(3, 'IO', 'Investments OneTime', NULL, 3, NULL );

INSERT INTO "PurchaseTypes" VALUES(4, 'CI', 'Car Insurance', NULL, 4, NULL );

INSERT INTO "PurchaseTypes" VALUES(5, 'PI', 'Property Insurance', NULL, 5, NULL );

INSERT INTO "PurchaseTypes" VALUES(6, 'MT', 'Mortgages', NULL, 6, NULL );

INSERT INTO "PurchaseTypes" VALUES(7, 'BS', 'Building Savings', NULL, 7, NULL );

INSERT INTO "PurchaseTypes" VALUES(8, 'PE', 'Pension Insurance', NULL, 8, NULL );

INSERT INTO "PurchaseTypes" VALUES(9, 'LO', 'Other Loans', NULL, 9, NULL );

--CREATE TABLE of product groups for each contract entity
CREATE TABLE "ContractProductGroups" as
SELECT DISTINCT
ccs."Code"||ccs."ServiceID" AS "ContractEntityID",
ccs."ServiceID",
ccat."CategoryID",
CASE WHEN ccat."ParentCategoryID" = '10300' THEN 'LI'
     WHEN ccat."ParentCategoryID" = '10500' AND cctx."PeriodAID" IN ('2','3','4','5') THEN 'IR'
     WHEN ccat."ParentCategoryID" = '10500' AND cctx."PeriodCID" IN ('1') THEN 'IO'
     WHEN ccat."CategoryID" IN ('10415','10416') THEN 'CI'
     WHEN ccat."CategoryID" IN ('10405') THEN 'PI'
     WHEN ccat."CategoryID" IN ('10211') THEN 'MT'
     WHEN ccat."CategoryID" IN ('10101') THEN 'BS'
     WHEN ccat."CategoryID" IN ('100029','10102') THEN 'PE'
     WHEN ccat."ParentCategoryID" = '10200' AND ccat."CategoryID" <>'10211' THEN 'LO'
     else 'OT' --others
     END AS "ProductGroup"
FROM "Customer_Contracts" ccs 
JOIN "Customer_ContractTrx" cctx ON ccs."ContractID" = cctx."ContractID"
JOIN "Biz_Services" bs ON ccs."ServiceID" = bs."ServiceID"
JOIN "Com_Categories" ccat ON bs."CategoryID" = ccat."CategoryID"
WHERE ccs."ContractTypeCode" IN ('NS','ZS')
AND cctx."ContractTrxTypeCode" = 'ZA'
;

-------------------------
-----family purchase-----
-------------------------
--v téhle tabulce je ke každému klientovi jaké produkty má jeho rodina, vyjma jeho samotného
CREATE TABLE "Family_Products" as
SELECT DISTINCT fc."ClientID",
cpg."ProductGroup"

FROM 
"FamilyClients" fc 
JOIN "FamilyClients" fc2 ON fc."FamilyID" = fc2."FamilyID"
JOIN "Customer_Clients" cc ON fc2."ClientID" = cc."ClientID"
JOIN "Customer_Contracts" ccs ON fc2."ClientID" = ccs."ClientID"
JOIN "ContractProductGroups" cpg ON ccs."Code"||ccs."ServiceID" = cpg."ContractEntityID"
JOIN "ContractActivity" ca ON ccs."Code"||ccs."ServiceID" = ca."Code"||ca."ServiceID"

WHERE fc."ClientID" <> fc2."ClientID"
AND cc."BirthDate" < dateadd(year, -18, current_date)
AND ca."Active" IN ('1','2')
;

---------------------
--ContractEntityTrx--
---------------------
CREATE TABLE "ContractEntityTrx" as
SELECT 
ccs."Code"||ccs."ServiceID" AS "ContractEntityID",
cctx."ContractTrxID"

FROM "Customer_Contracts" ccs
JOIN "Customer_ContractTrx" cctx ON ccs."ContractID" = cctx."ContractID"
;

--Amount tmp table for amount AND period for each trx, THEN used for SELECTing first trx for contract entity
CREATE TABLE "tmp_amounts" as
SELECT cet."ContractEntityID",
--přidat CASE na stavebko na částku C
CASE WHEN ccat."ParentCategoryID" IN ('10300','10400','10100') THEN ifnull(cctx."AmountA",0)+ifnull(cctx."AmountB",0)
WHEN ccat."ParentCategoryID" = '10200' THEN cctx."AmountC"
WHEN ccat."ParentCategoryID" = '10500' THEN ifnull(cctx."AmountA", cctx."AmountC")
END AS "Amount",
CASE WHEN ccat."ParentCategoryID" IN ('10300','10400','10100') THEN ccp."Name"
WHEN ccat."ParentCategoryID" = '10200' THEN ccb."Name"
WHEN ccat."ParentCategoryID" = '10500' THEN ifnull(ccp."Name", ccb."Name")
END AS "Period",
cctx."DateCreated"

FROM "ContractEntityTrx" cet
JOIN "Customer_ContractTrx" cctx ON cet."ContractTrxID" = cctx."ContractTrxID"
JOIN "Customer_Contracts" ccs ON cctx."ContractID" = ccs."ContractID"
JOIN "Biz_Services" bs ON ccs."ServiceID" = bs."ServiceID"
JOIN "Com_Categories" ccat ON bs."CategoryID" = ccat."CategoryID"
LEFT OUTER JOIN "Customer_ContractPeriods" ccp ON cctx."PeriodAID" = ccp."PeriodID"
LEFT OUTER JOIN "Customer_ContractBasePeriods" ccb ON cctx."PeriodCID" = ccb."PeriodID"
LEFT OUTER JOIN "Customer_ContractBasePeriods" ccb2 ON cctx."PeriodDID" = ccb2."PeriodID"            
;

--contract entity
CREATE TABLE "ContractEntity" as
SELECT
DISTINCT
ccs."Code"||ccs."ServiceID" AS "ContractEntityID",
current_date AS "DateUpdated",
null AS "UpdatedBy",
ccs."ClientID",
ccs."ServiceID",
bs."PartnerID",
da."DateSigned",
da."DateEffective",
da."DateExpire",
ca."Active" AS "ContractPhase",
ifnull(tp."TotalPoints",0) AS "BusinessPoints",
ifnull(fup."FollowUpPoints",0) AS "FollowUpPoints",
ap."Amount",
ap."Period" AS "PeriodID",
fc."FamilyID" AS "PartyContactID"


FROM "Customer_Contracts" ccs
JOIN "Biz_Services" bs ON ccs."ServiceID" = bs."ServiceID"
JOIN "ContractEntityTrx" cet ON ccs."Code"||ccs."ServiceID"=cet."ContractEntityID"
JOIN "ContractActivity" ca ON cet."ContractEntityID" = ca."Code"||ca."ServiceID"
JOIN "FamilyClients" fc ON ccs."ClientID" = fc."ClientID"
--TotalPoints
LEFT OUTER JOIN (SELECT sum(cctx."BusinessPoints") AS "TotalPoints", ccs."Code"||ccs."ServiceID" AS "ContractEntityID"
           FROM "Customer_Contracts" ccs
           JOIN "Customer_ContractTrx" cctx ON ccs."ContractID" = cctx."ContractID"
           WHERE ccs."ContractTypeCode" <> 'NP'
           GROUP BY ccs."Code"||ccs."ServiceID"
           )tp ON cet."ContractEntityID" = tp."ContractEntityID"

--FollowUpPoints
LEFT OUTER JOIN (SELECT sum(cctx."BusinessPoints") AS "FollowUpPoints", ccs."Code"||ccs."ServiceID" AS "ContractEntityID"
           FROM "Customer_Contracts" ccs
           JOIN "Customer_ContractTrx" cctx ON ccs."ContractID" = cctx."ContractID"
           WHERE ccs."ContractTypeCode" = 'NP'
           GROUP BY ccs."Code"||ccs."ServiceID"
           )fup ON cet."ContractEntityID" = fup."ContractEntityID"

--Amount AND period
LEFT OUTER JOIN (SELECT DISTINCT ta."ContractEntityID", 
            FIRST_VALUE(ta."Amount") OVER (PARTITION BY ta."ContractEntityID" ORDER BY ta."DateCreated") AS "Amount",
            FIRST_VALUE(ta."Period") OVER (PARTITION BY ta."ContractEntityID" ORDER BY ta."DateCreated") AS "Period"
            FROM
            "tmp_amounts" ta)ap ON cet."ContractEntityID" = ap."ContractEntityID"

--dates
LEFT OUTER JOIN (SELECT DISTINCT cet."ContractEntityID",
                 FIRST_VALUE(ccs."DateSigned") OVER (PARTITION BY cet."ContractEntityID" ORDER BY ccs."DateSigned") AS "DateSigned",
                 FIRST_VALUE(ccs."DateEffective") OVER (PARTITION BY cet."ContractEntityID" ORDER BY ccs."DateSigned") AS "DateEffective",
                 FIRST_VALUE(ccs."DateExpire") OVER (PARTITION BY cet."ContractEntityID" ORDER BY ccs."DateSigned") AS "DateExpire"
                 FROM "ContractEntityTrx" cet
                 JOIN "Customer_Contracts" ccs ON cet."ContractEntityID" = ccs."Code"||ccs."ServiceID"
                 )da ON cet."ContractEntityID" = da."ContractEntityID"
                 
;

------------------------
--ContractEntityAgents--
------------------------
CREATE TABLE "ContractEntityAgents" AS 
SELECT ce."ContractEntityID",
ccta."AgentID",
ccta."ContractTrxAgentTypeID",
floor(sum(cctx."BusinessPoints" * ccta."PercentShare"/100)/sum(cctx."BusinessPoints")*100,2) AS "PercentShare",
sum(cctx."BusinessPoints" * ccta."PercentShare"/100) AS "BusinessPoints",
s1."FollowUpPoints"

FROM "ContractEntity" ce
JOIN "Customer_Contracts" ccs ON ce."ContractEntityID" = ccs."Code"||ccs."ServiceID"
JOIN "Customer_ContractTrx" cctx ON ccs."ContractID" = cctx."ContractID"
JOIN "Customer_ContractTrxAgents" ccta ON cctx."ContractTrxID" = ccta."ContractTrxID"
LEFT OUTER JOIN (SELECT ce."ContractEntityID",
                ccta."AgentID",
                ccta."ContractTrxAgentTypeID",
                sum(cctx."BusinessPoints" * ccta."PercentShare"/100) AS "FollowUpPoints"
                FROM "ContractEntity" ce
                JOIN "Customer_Contracts" ccs ON ce."ContractEntityID" = ccs."Code"||ccs."ServiceID"
                JOIN "Customer_ContractTrx" cctx ON ccs."ContractID" = cctx."ContractID"
                JOIN "Customer_ContractTrxAgents" ccta ON cctx."ContractTrxID" = ccta."ContractTrxID"
                WHERE ccs."ContractTypeCode" = 'NP'
                AND ccta."PercentShare" <> 0
                GROUP BY ce."ContractEntityID", ccta."AgentID", ccta."ContractTrxAgentTypeID"
                )s1 ON ce."ContractEntityID" =  s1."ContractEntityID" AND ccta."AgentID" = s1."AgentID"

WHERE ccs."ContractTypeCode" <> 'NP'
--AND ccs."Code" = '1108057'
AND ccta."PercentShare" <> 0
GROUP BY ce."ContractEntityID", ccta."AgentID",ccta."ContractTrxAgentTypeID", s1."FollowUpPoints"
HAVING sum(cctx."BusinessPoints")<>0;

-------------------
--ClientPurchases--
-------------------
--this works AS SELECTing last active contract for each product category for each customer, 
--if no contract THEN predicted points FROM some R transformation should be inserted, WHERE
--it does not make sense, there will be some indications (such AS mortgage for U18 customer)

-- tohle jeste asi budu upravovat protoze v navrhu to je jinak a ma
-- tam byt i contractentityid, coz ale nevim jestli dava smysl

CREATE TABLE "ClientPurchase" as
SELECT cc."ClientID", 
to_char(f.Value) AS "PurchaseTypeID", 
NULL AS "NumericValue",
NULL AS "DateTimeValue", 
NULL AS "StringValue"
FROM 
"Customer_Clients" cc,
TABLE(FLATTEN(INPUT =>PARSE_JSON('["LI","MT","IR","IO","CI","PI","BS","PE","LO"]'))) f;

--formerly this was temp, so check it for corectness
CREATE TABLE "tmp_last_contract" AS 
SELECT DISTINCT ce."ClientID",
cpg."ProductGroup" AS "PurchaseTypeID",
FIRST_VALUE(ce."DateSigned") OVER (PARTITION BY ce."ClientID", cpg."ProductGroup" ORDER BY ce."DateSigned" desc) AS "DateTimeValue",
NULL AS "NumericValue",
NULL AS "StringValue",
ce."ContractEntityID"
FROM "ContractEntity" ce
JOIN "ContractProductGroups" cpg ON ce."ContractEntityID" = cpg."ContractEntityID"
--JOIN "Biz_Services" bs ON ce."ServiceID" = bs."ServiceID"
--JOIN "Com_Categories" ccat ON bs."CategoryID" = ccat."CategoryID"
WHERE ce."ContractPhase" IN ('1','2')
;

--insert last contract date into client purchases
UPDATE "ClientPurchase" p
SET p."DateTimeValue" =  tt."DateTimeValue" FROM "tmp_last_contract" tt 
                      WHERE p."PurchaseTypeID" = tt."PurchaseTypeID" AND p."ClientID" = tt."ClientID"
;

--drop table "temp_exp_reve";
CREATE TABLE "temp_exp_reve" as
SELECT 
"ClientID",
"LI",
"MT",
"IR",
"IO",
"CI",
"PI",
"BS",
"PE",
"LO"
FROM "ClientRevenue";

UPDATE "ClientPurchase" p
SET p."NumericValue" =  tt."LI" FROM "temp_exp_reve" tt 
                      WHERE p."ClientID" = tt."ClientID" AND p."PurchaseTypeID" = 'LI'
;

UPDATE "ClientPurchase" p
SET p."NumericValue" =  tt."MT" FROM "temp_exp_reve" tt 
                      WHERE p."ClientID" = tt."ClientID" AND p."PurchaseTypeID" = 'MT'
;

UPDATE "ClientPurchase" p
SET p."NumericValue" =  tt."IR" FROM "temp_exp_reve" tt 
                      WHERE p."ClientID" = tt."ClientID" AND p."PurchaseTypeID" = 'IR'
;

UPDATE "ClientPurchase" p
SET p."NumericValue" =  tt."IO" FROM "temp_exp_reve" tt 
                      WHERE p."ClientID" = tt."ClientID" AND p."PurchaseTypeID" = 'IO'
;

UPDATE "ClientPurchase" p
SET p."NumericValue" =  tt."CI" FROM "temp_exp_reve" tt 
                      WHERE p."ClientID" = tt."ClientID" AND p."PurchaseTypeID" = 'CI'
;

UPDATE "ClientPurchase" p
SET p."NumericValue" =  tt."PI" FROM "temp_exp_reve" tt 
                      WHERE p."ClientID" = tt."ClientID" AND p."PurchaseTypeID" = 'PI'
;

UPDATE "ClientPurchase" p
SET p."NumericValue" =  tt."BS" FROM "temp_exp_reve" tt 
                      WHERE p."ClientID" = tt."ClientID" AND p."PurchaseTypeID" = 'BS'
;

UPDATE "ClientPurchase" p
SET p."NumericValue" =  tt."PE" FROM "temp_exp_reve" tt 
                      WHERE p."ClientID" = tt."ClientID" AND p."PurchaseTypeID" = 'PE'
;

UPDATE "ClientPurchase" p
SET p."NumericValue" =  tt."PE" FROM "temp_exp_reve" tt 
                      WHERE p."ClientID" = tt."ClientID" AND p."PurchaseTypeID" = 'LO'
;

--Rules for StringValue:
--Customer under 18 - no loans, no non-life insurace
--Customer under 18 with parents - no life insurnace
--Customer OVER 65 - no loans
--Customer OVER 18 with partner having life, non life or mortgage - nothing
--drop table "temp_s";
CREATE TABLE "temp_s" as
SELECT cc."ClientID", 
to_char(f.Value) AS "ProductGroup", 
CASE WHEN cc."BirthDate">'1990-01-01' THEN 'Dítě'
     WHEN cc."BirthDate"<'1953-01-01' THEN 'Senior' END AS "StringValue"
FROM 
"Customer_Clients" cc,
table(flatten(input =>parse_json('["LI","MT","IR","IO","CI","PI","BS","PE", "LO"]'))) f;

UPDATE "temp_s" s
SET s."StringValue" = 'Hypotéka v rodině' FROM "Family_Products" pt 
                                          WHERE s."ClientID" = pt."ClientID" AND s."ProductGroup" = pt."ProductGroup"
                                          AND s."ProductGroup" = 'MT';

UPDATE "temp_s" s
SET s."StringValue" = 'Úvěr v rodině' FROM "Family_Products" pt 
                                          WHERE s."ClientID" = pt."ClientID" AND s."ProductGroup" = pt."ProductGroup"
                                          AND s."ProductGroup" = 'LO';

UPDATE "temp_s" s
SET s."StringValue" = 'Pojištění majetku v rodině' FROM "Family_Products" pt 
                                          WHERE s."ClientID" = pt."ClientID" AND s."ProductGroup" = pt."ProductGroup"
                                          AND s."ProductGroup" = 'PI';

--Insert string values into client purchases
UPDATE "ClientPurchase" p
SET p."StringValue" =  s."StringValue" FROM "temp_s" s 
                      WHERE p."ClientID" = s."ClientID" AND p."PurchaseTypeID" = s."ProductGroup"
;

--Replace PurchaseTypeID with number
UPDATE "ClientPurchase" p
SET p."PurchaseTypeID" = p2."PurchaseTypeID" FROM "PurchaseTypes" p2
                        WHERE p."PurchaseTypeID" = p2."Name"
;

------------
--Families--
------------
CREATE TABLE "Families" AS
SELECT DISTINCT
fc."FamilyID",
cc."ClientID" AS "HeadClientID",
lc."LastContract",
uniform(1,5, random()) AS "Rating",
p."Potential",
uniform(0::float,1::float, random()) AS "ProbabilityOfPurchase",
sum(hp."HistoricalPoints") AS "HistoricalPoints",
-hc."HistoricalCancellation" / hr."HistoricalRevenue" AS "HistoricalCancellation",
current_date AS "LastUpdated"

FROM "FamilyClients" fc
--JOIN C
LEFT OUTER JOIN (SELECT ccs."ClientID", sum(cctx."BusinessPoints") AS "HistoricalPoints" 
                 FROM "Customer_Contracts" ccs
                 JOIN "Customer_ContractTrx" cctx ON ccs."ContractID" = cctx."ContractID"
                 WHERE cctx."ContractTrxStatusCode" not IN ('N','O','F')
                 GROUP BY ccs."ClientID")hp ON fc."ClientID" = hp."ClientID"

LEFT OUTER JOIN (SELECT fc."FamilyID", sum(acr."Amount") AS "HistoricalRevenue" 
                 FROM "Customer_Contracts" ccs
                 JOIN "Customer_ContractTrx" cctx ON ccs."ContractID" = cctx."ContractID"
                 JOIN "Acct_Revenue" acr ON cctx."ContractTrxID" = acr."ContractTrxID"
                 JOIN "FamilyClients" fc ON ccs."ClientID" = fc."ClientID"
                 WHERE acr."Amount" > 0
                 GROUP BY fc."FamilyID")hr ON fc."FamilyID" = hr."FamilyID"

LEFT OUTER JOIN (SELECT fc."FamilyID", sum(acr."Amount") AS "HistoricalCancellation" 
                 FROM "Customer_Contracts" ccs
                 JOIN "Customer_ContractTrx" cctx ON ccs."ContractID" = cctx."ContractID"
                 JOIN "Acct_Revenue" acr ON cctx."ContractTrxID" = acr."ContractTrxID"
                 JOIN "FamilyClients" fc ON ccs."ClientID" = fc."ClientID"
                 WHERE acr."Amount" < 0
                 GROUP BY fc."FamilyID")hc ON fc."FamilyID" = hc."FamilyID"

LEFT OUTER JOIN (SELECT fc."FamilyID", max(ccs."DateSigned") AS "LastContract"
                 FROM "Customer_Contracts" ccs
                 JOIN "FamilyClients" fc ON ccs."ClientID" = fc."ClientID"
                 GROUP BY fc."FamilyID")lc ON fc."FamilyID" = lc."FamilyID"

LEFT OUTER JOIN (SELECT fc."FamilyID", sum(p."NumericValue") AS "Potential" FROM "ClientPurchase" p
                 JOIN "FamilyClients" fc ON p."ClientID" = fc."ClientID"
                 WHERE p."DateTimeValue" IS NULL
                 AND p."StringValue" IS NULL
                 GROUP BY fc."FamilyID")p ON fc."FamilyID" = p."FamilyID"
JOIN "Customer_Clients" cc on fc."FamilyID" = cc."PartyID"

GROUP BY fc."FamilyID",cc."ClientID", lc."LastContract", hc."HistoricalCancellation", hr."HistoricalRevenue", p."Potential"
;

CREATE TABLE "tmp_tickets" AS
SELECT distinct ce."ContractEntityID",
--fc."FamilyID",
dateadd('year',
        datediff('year',ce."DateEffective",current_date) + 1,
        dateadd('month',case when ccat."ParentCategoryID" in ('10300','10400') then -3 else -1 end ,ce."DateEffective")
       ) as "Date"
--ce."DateEffective",
FROM "ContractEntity" ce
JOIN "Biz_Services" bs on ce."ServiceID" = bs."ServiceID"
JOIN "Com_Categories" ccat on bs."CategoryID" = ccat."CategoryID"
--JOIN "FamilyClients" fc on ce."ClientID" = fc."ClientID"
--where ce."ContractPhase" = '1'
;

CREATE TABLE "ClientTickets" AS
SELECT datediff('day','2018-01-01',t."Date")||t2."rnk" as "ClientTicketID",
t."ContractEntityID",
fc."FamilyID",
t."Date" 
FROM "tmp_tickets" t
JOIN (select "Date", "ContractEntityID", rank() over(partition by "Date" order by "ContractEntityID") as "rnk" 
        from "tmp_tickets")t2 on t."ContractEntityID" = t2."ContractEntityID"
JOIN "ContractEntity" ce on t."ContractEntityID" = ce."ContractEntityID"
JOIN "FamilyClients" fc on ce."ClientID" = fc."ClientID"
;