--------------
-----kids-----
--------------
create table "to_r_kids" as
select distinct cc."ClientID",
tr."Amount" as "TotalReve",
bs."Amount" as "Build",
pe."Amount" as "Pension",
ir."Amount" as "Inv",
job."JobRank"

from "Customer_Clients" cc
join "FamilyClients" fc on cc."ClientID" = fc."ClientID"
--celkové revenues
left outer join (select sum(acr."Amount") as "Amount", ccs."ClientID" from
                 "Acct_Revenue" acr
                 join "Customer_ContractTrx" cctx on acr."ContractTrxID" = cctx."ContractTrxID"
                 join "Customer_Contracts" ccs on cctx."ContractID" = ccs."ContractID"
                 group by ccs."ClientID")tr on cc."ClientID" = tr."ClientID"

--stavebko
left outer join (select sum(acr."Amount") as "Amount", ccs."ClientID" from
                 "Acct_Revenue" acr
                 join "Customer_ContractTrx" cctx on acr."ContractTrxID" = cctx."ContractTrxID"
                 join "Customer_Contracts" ccs on cctx."ContractID" = ccs."ContractID"
                 join "Biz_Services" bs on ccs."ServiceID" = bs."ServiceID"
                 where bs."CategoryID" = '10101'
                 group by ccs."ClientID")bs on cc."ClientID" = bs."ClientID"


--penze
left outer join (select sum(acr."Amount") as "Amount", ccs."ClientID" from
                 "Acct_Revenue" acr
                 join "Customer_ContractTrx" cctx on acr."ContractTrxID" = cctx."ContractTrxID"
                 join "Customer_Contracts" ccs on cctx."ContractID" = ccs."ContractID"
                 join "Biz_Services" bs on ccs."ServiceID" = bs."ServiceID"
                 where bs."CategoryID" in ('100029','10102')
                 group by ccs."ClientID")pe on cc."ClientID" = pe."ClientID"

--investice pravidelné - teď podle transakce, ale později přidat podle contract entity
left outer join (select sum(acr."Amount") as "Amount", ccs."ClientID" from
                 "Acct_Revenue" acr
                 join "Customer_ContractTrx" cctx on acr."ContractTrxID" = cctx."ContractTrxID"
                 join "Customer_Contracts" ccs on cctx."ContractID" = ccs."ContractID"
                 join "Biz_Services" bs on ccs."ServiceID" = bs."ServiceID"
                 join "Com_Categories" ccat on bs."CategoryID" = ccat."CategoryID"
                 where ccat."ParentCategoryID" in ('10500')
                 and cctx."PeriodAID" in ('2','3','4','5')
                 group by ccs."ClientID")ir on cc."ClientID" = ir."ClientID"

--nejlepší job z rodiny
left outer join (select f."FamilyID",
                 first_value(cj."JobRank") over (partition by f."FamilyID" order by cj."JobRank") as "JobRank"
                 from "FamilyClients" f
                 join "ClientJobs" cj on f."ClientID" = cj."ClientID")job on fc."FamilyID" = job."FamilyID"



where cc."BirthDate" >=dateadd(year, -18, current_date)
and tr."Amount" is not null
;

--drop table "product_tmp";
-------------------------
-----family purchase-----
-------------------------
--v téhle tabulce je ke každému klientovi jaké produkty má jeho rodina, vyjma jeho samotného
create table "product_tmp" as
select distinct fc."ClientID",
case when ccat."ParentCategoryID" = '10300' then 'LI'
     when ccat."ParentCategoryID" = '10500' then 'Inv'
     when ccat."CategoryID" in ('10415','10416') then 'CI'
     when ccat."CategoryID" in ('10405') then 'PI'
     when ccat."CategoryID" in ('10211') then 'Mort'
     when ccat."CategoryID" in ('10101') then 'BS'
     when ccat."CategoryID" in ('100029','10102') then 'Pension'
     when ccat."ParentCategoryID" = '10200' and ccat."CategoryID" <>'10211' then 'Loans'
     end as "Product",
sum(acr."Amount") as "Amount"
from 
"FamilyClients" fc 
join "FamilyClients" fc2 on fc."FamilyID" = fc2."FamilyID"
join "Customer_Clients" cc on fc2."ClientID" = cc."ClientID"
join "Customer_Contracts" ccs on fc2."ClientID" = ccs."ClientID"
join "Customer_ContractTrx" cctx on ccs."ContractID" = cctx."ContractID"
join "Acct_Revenue" acr on cctx."ContractTrxID" = acr."ContractTrxID"
join "Biz_Services" bs on ccs."ServiceID" = bs."ServiceID"
join "Com_Categories" ccat on bs."CategoryID" = ccat."CategoryID"
where fc."ClientID" <> fc2."ClientID"
and cc."BirthDate" < dateadd(year, -18, current_date)
and ccs."ContractTypeCode" in ('NS','ZS')
and cctx."ContractTrxTypeCode" = 'ZA'
group by fc."ClientID", case when ccat."ParentCategoryID" = '10300' then 'LI'
     when ccat."ParentCategoryID" = '10500' then 'Inv'
     when ccat."CategoryID" in ('10415','10416') then 'CI'
     when ccat."CategoryID" in ('10405') then 'PI'
     when ccat."CategoryID" in ('10211') then 'Mort'
     when ccat."CategoryID" in ('10101') then 'BS'
     when ccat."CategoryID" in ('100029','10102') then 'Pension'
     when ccat."ParentCategoryID" = '10200' and ccat."CategoryID" <>'10211' then 'Loans'
     end

;

--rozdělit podle věku
--podle toho co již mají v rodině
--podle bonity rodiny?

--------------
----adults----
--------------
create table "to_r_adults" as
select distinct cc."ClientID",
ifnull(tr."Amount",0) as "TotalReve",
ifnull(li."Amount",0) as "LI",
ifnull(ir."Amount",0) as "IR",
ifnull(io."Amount",0) as "IO",
ifnull(ci."Amount",0) as "CI",
ifnull(pri."Amount",0) as "PI",
ifnull(mt."Amount",0) as "MT",
ifnull(bs."Amount",0) as "BS",
ifnull(pe."Amount",0) as "PE",
ifnull(lo."Amount",0) as "LO",
ifnull(pli."Amount",0) as "FamilyLI",
ifnull(pin."Amount",0) as "FamilyInv",
ifnull(pci."Amount",0) as "FamilyCI",
ifnull(ppi."Amount",0) as "FamilyPI",
ifnull(pmt."Amount",0) as "FamilyMort",
ifnull(plo."Amount",0) as "FamilyLoans",
job."JobRank" as "JobRank",
minbp."MinBP"


from "Customer_Clients" cc
join "FamilyClients" fc on cc."ClientID" = fc."ClientID"
--celkové revenues
left outer join (select sum(acr."Amount") as "Amount", ccs."ClientID" from
                 "Acct_Revenue" acr
                 join "Customer_ContractTrx" cctx on acr."ContractTrxID" = cctx."ContractTrxID"
                 join "Customer_Contracts" ccs on cctx."ContractID" = ccs."ContractID"
                 where ccs."ContractTypeCode" in ('NS','ZS')
                 and cctx."ContractTrxTypeCode" = 'ZA'
                 group by ccs."ClientID")tr on cc."ClientID" = tr."ClientID"

--žp
left outer join (select sum(acr."Amount") as "Amount", ccs."ClientID" from
                 "Acct_Revenue" acr
                 join "Customer_ContractTrx" cctx on acr."ContractTrxID" = cctx."ContractTrxID"
                 join "Customer_Contracts" ccs on cctx."ContractID" = ccs."ContractID"
                 join "Biz_Services" bs on ccs."ServiceID" = bs."ServiceID"
                 join "Com_Categories" ccat on bs."CategoryID" = ccat."CategoryID"
                 where ccat."ParentCategoryID" = '10300'
                 and ccs."ContractTypeCode" in ('NS','ZS')
                 and cctx."ContractTrxTypeCode" = 'ZA'
                 group by ccs."ClientID")li on cc."ClientID" = li."ClientID"

--investice pravidelné - teď podle transakce, ale později přidat podle contract entity
left outer join (select sum(acr."Amount") as "Amount", ccs."ClientID" from
                 "Acct_Revenue" acr
                 join "Customer_ContractTrx" cctx on acr."ContractTrxID" = cctx."ContractTrxID"
                 join "Customer_Contracts" ccs on cctx."ContractID" = ccs."ContractID"
                 join "Biz_Services" bs on ccs."ServiceID" = bs."ServiceID"
                 join "Com_Categories" ccat on bs."CategoryID" = ccat."CategoryID"
                 where ccat."ParentCategoryID" in ('10500')
                 and cctx."PeriodAID" in ('2','3','4','5')
                 and ccs."ContractTypeCode" in ('NS','ZS')
                 and cctx."ContractTrxTypeCode" = 'ZA'
                 group by ccs."ClientID")ir on cc."ClientID" = ir."ClientID"

--investice jednorázové
left outer join (select sum(acr."Amount") as "Amount", ccs."ClientID" from
                 "Acct_Revenue" acr
                 join "Customer_ContractTrx" cctx on acr."ContractTrxID" = cctx."ContractTrxID"
                 join "Customer_Contracts" ccs on cctx."ContractID" = ccs."ContractID"
                 join "Biz_Services" bs on ccs."ServiceID" = bs."ServiceID"
                 join "Com_Categories" ccat on bs."CategoryID" = ccat."CategoryID"
                 where ccat."ParentCategoryID" in ('10500')
                 and cctx."PeriodAID" in ('1')
                 and ccs."ContractTypeCode" in ('NS','ZS')
                 and cctx."ContractTrxTypeCode" = 'ZA'
                 group by ccs."ClientID")io on cc."ClientID" = io."ClientID"

--pov/hav
left outer join (select sum(acr."Amount") as "Amount", ccs."ClientID" from
                 "Acct_Revenue" acr
                 join "Customer_ContractTrx" cctx on acr."ContractTrxID" = cctx."ContractTrxID"
                 join "Customer_Contracts" ccs on cctx."ContractID" = ccs."ContractID"
                 join "Biz_Services" bs on ccs."ServiceID" = bs."ServiceID"
                 where bs."CategoryID" in ('10415','10416')
                 and ccs."ContractTypeCode" in ('NS','ZS')
                 and cctx."ContractTrxTypeCode" = 'ZA'
                 group by ccs."ClientID")ci on cc."ClientID" = ci."ClientID"

--majetek
left outer join (select sum(acr."Amount") as "Amount", ccs."ClientID" from
                 "Acct_Revenue" acr
                 join "Customer_ContractTrx" cctx on acr."ContractTrxID" = cctx."ContractTrxID"
                 join "Customer_Contracts" ccs on cctx."ContractID" = ccs."ContractID"
                 join "Biz_Services" bs on ccs."ServiceID" = bs."ServiceID"
                 where bs."CategoryID" in ('10405')
                 and ccs."ContractTypeCode" in ('NS','ZS')
                 and cctx."ContractTrxTypeCode" = 'ZA'
                 group by ccs."ClientID")pri on cc."ClientID" = pri."ClientID"



--hypo
left outer join (select sum(acr."Amount") as "Amount", ccs."ClientID" from
                 "Acct_Revenue" acr
                 join "Customer_ContractTrx" cctx on acr."ContractTrxID" = cctx."ContractTrxID"
                 join "Customer_Contracts" ccs on cctx."ContractID" = ccs."ContractID"
                 join "Biz_Services" bs on ccs."ServiceID" = bs."ServiceID"
                 where bs."CategoryID" = '10211'
                 and ccs."ContractTypeCode" in ('NS','ZS')
                 and cctx."ContractTrxTypeCode" = 'ZA'
                 group by ccs."ClientID")mt on cc."ClientID" = mt."ClientID"




--stavebko
left outer join (select sum(acr."Amount") as "Amount", ccs."ClientID" from
                 "Acct_Revenue" acr
                 join "Customer_ContractTrx" cctx on acr."ContractTrxID" = cctx."ContractTrxID"
                 join "Customer_Contracts" ccs on cctx."ContractID" = ccs."ContractID"
                 join "Biz_Services" bs on ccs."ServiceID" = bs."ServiceID"
                 where bs."CategoryID" = '10101'
                 and ccs."ContractTypeCode" in ('NS','ZS')
                 and cctx."ContractTrxTypeCode" = 'ZA'
                 group by ccs."ClientID")bs on cc."ClientID" = bs."ClientID"


--penze
left outer join (select sum(acr."Amount") as "Amount", ccs."ClientID" from
                 "Acct_Revenue" acr
                 join "Customer_ContractTrx" cctx on acr."ContractTrxID" = cctx."ContractTrxID"
                 join "Customer_Contracts" ccs on cctx."ContractID" = ccs."ContractID"
                 join "Biz_Services" bs on ccs."ServiceID" = bs."ServiceID"
                 where bs."CategoryID" in ('100029','10102')
                 and ccs."ContractTypeCode" in ('NS','ZS')
                 and cctx."ContractTrxTypeCode" = 'ZA'
                 group by ccs."ClientID")pe on cc."ClientID" = pe."ClientID"
--uvery
left outer join (select sum(acr."Amount") as "Amount", ccs."ClientID" from
                 "Acct_Revenue" acr
                 join "Customer_ContractTrx" cctx on acr."ContractTrxID" = cctx."ContractTrxID"
                 join "Customer_Contracts" ccs on cctx."ContractID" = ccs."ContractID"
                 join "Biz_Services" bs on ccs."ServiceID" = bs."ServiceID"
                 join "Com_Categories" ccat on bs."CategoryID" = ccat."CategoryID"
                 where ccat."ParentCategoryID" = '10200' 
                 and bs."CategoryID" <>'10211'
                 and ccs."ContractTypeCode" in ('NS','ZS')
                 and cctx."ContractTrxTypeCode" = 'ZA'
                 group by ccs."ClientID")lo on cc."ClientID" = lo."ClientID"

--rodiná má žp
left outer join "product_tmp" pli on cc."ClientID" = pli."ClientID" and pli."Product" = 'LI'

--rodina má inv
left outer join "product_tmp" pin on cc."ClientID" = pin."ClientID" and pin."Product" = 'Inv'

--rodina má pov/hav
left outer join "product_tmp" pci on cc."ClientID" = pci."ClientID" and pci."Product" = 'CI'

--rodina má majetek
left outer join "product_tmp" ppi on cc."ClientID" = ppi."ClientID" and ppi."Product" = 'PI'

--rodina má hypo
left outer join "product_tmp" pmt on cc."ClientID" = pmt."ClientID" and pmt."Product" = 'Mort'

--rodina má úvěr
left outer join "product_tmp" plo on cc."ClientID" = plo."ClientID" and ppi."Product" = 'Loans'



--nejlepší job z rodiny
left outer join (select f."FamilyID",
                 first_value(cj."JobRank") over (partition by f."FamilyID" order by cj."JobRank") as "JobRank"
                 from "FamilyClients" f
                 join "ClientJobs" cj on f."ClientID" = cj."ClientID")job on fc."FamilyID" = job."FamilyID"


--stáří klienta
left outer join (select ccs."ClientID", min(cctx."BusinessPeriodID") as "MinBP"
                 from "Customer_Contracts" ccs
                 join "Customer_ContractTrx" cctx on ccs."ContractID" = cctx."ContractID"
                 where ccs."ContractTypeCode" in ('NS','ZS')
                 and cctx."ContractTrxTypeCode" = 'ZA'
                 group by ccs."ClientID"
                 )minbp on cc."ClientID" = minbp."ClientID"

where 
cc."BirthDate" < dateadd(year, -18, current_date)
and cc."BirthDate" > dateadd(year, -65, current_date)

and tr."Amount" is not null
--and tr."Amount"<250000 --odseknout lidi nad 250k - pak řešit nějak jinak
--order by "TotalReve" desc
;