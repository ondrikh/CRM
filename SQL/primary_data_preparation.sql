SET currentdate=(SELECT date_trunc('month',current_date));

SET bp = (SELECT "BusinessPeriodID" FROM "Agent_BusinessPeriods" 
WHERE date_trunc('month',current_date) = "DateFrom")
;

CREATE TABLE "active" 
AS
-----------------------------------------------ŽIVOT----------------------------------------------

SELECT 
DISTINCT
ccs."Code",
bs."ServiceID",
    
CASE WHEN cctx."ContractTrxStatusCode" = 'ZO' AND zp."Amount" IS NULL THEN '2'
            WHEN s1."BB"=0 AND np."Amount" IS NULL THEN '0'
            WHEN s1."BB"<>0 or (s1."BB"=0 AND np."Amount" IS NOT NULL) THEN
                CASE WHEN zp."Amount"<0 AND np."Amount" IS NULL THEN '0'
                     WHEN zp."Amount"<0 AND np."Amount" IS NOT NULL THEN
                        CASE WHEN np."Amount">0 THEN	
                            CASE WHEN np."DateCreated"<zp."DateCreated" THEN '0'
                                 WHEN np."DateCreated">zp."DateCreated" THEN
                                    CASE WHEN bs."FollowUpMaximum" IS NULL THEN
                                        CASE WHEN datediff(month,np."DateCreated", $currentdate) >16 THEN '0' ELSE '1' END
                                         WHEN bs."FollowUpMaximum" IS NOT NULL THEN
                                            CASE WHEN DATEADD(year,bs."FollowUpMaximum",ccs."DateEffective")<=$currentdate THEN '0'
                                                 WHEN DATEADD(year,bs."FollowUpMaximum",ccs."DateEffective")>$currentdate THEN
                                                    CASE WHEN datediff(month,np."DateCreated", $currentdate) >16 THEN '0' ELSE '1' END
                                            END
                                    END

                            END
                        WHEN np."Amount"<0 THEN '0'
                        END
                     WHEN zp."Amount">0 THEN
                        CASE WHEN np."Amount"<0 THEN '0'
                             WHEN np."Amount">0 or np."Amount" IS NULL THEN
                                CASE WHEN bs."FollowUp"=0 THEN '1'
                                     WHEN bs."FollowUp"=1 AND np."Amount">0 THEN
                                        CASE WHEN bs."FollowUpMaximum" IS NULL THEN --(510060683,510060997,510061003,510061001,510060998,510060648,510060999,510060994,510060995,510060986,510060985,510060988,510060987,510060989,510060990,510060971,510060970) THEN
                                                CASE WHEN datediff(month,np."DateCreated", $currentdate) >16 THEN '0' ELSE '1' END
                                             WHEN bs."FollowUpMaximum" IS NOT NULL THEN
                                                CASE WHEN DATEADD(year,bs."FollowUpMaximum",ccs."DateEffective")<=$currentdate THEN '0'
                                                     WHEN DATEADD(year,bs."FollowUpMaximum",ccs."DateEffective")>$currentdate THEN
                                                        CASE WHEN datediff(month,np."DateCreated", $currentdate) >16 THEN '0' ELSE '1' END
                                                END
                                        END
                                END
                        END
                END												
END AS "Active"

FROM "Customer_Contracts" ccs
JOIN "Customer_ContractTrx" cctx ON (ccs."ContractID"=cctx."ContractID")
JOIN "Biz_Services" bs ON (ccs."ServiceID"=bs."ServiceID")
JOIN "Com_Categories" ccat ON (bs."CategoryID"=ccat."CategoryID")



--smlouvy které mají nulové BB - nejčastěji fake smlouvy které jsou vyčištěné, může jít i o nahrazované smlouvy
LEFT JOIN (SELECT sum(cctx."BusinessPoints") AS BB, ccs."Code", ccs."ServiceID" FROM 
                "Customer_Contracts" ccs 
                JOIN "Customer_ContractTrx" cctx ON (ccs."ContractID"=cctx."ContractID")
                WHERE 
                cctx."ContractTrxStatusCode" NOT IN ('N','O','F')
                AND ccs."ContractTypeCode"<>'NP'
                GROUP BY ccs."Code", ccs."ServiceID")s1 ON (ccs."Code"=s1."Code" AND ccs."ServiceID"=s1."ServiceID")

--poslední provize (ne NP)
LEFT JOIN (SELECT ccs."Code", 
           ccs."ServiceID", 
           first_value(sum(acr."Amount")) OVER (partition by ccs."Code", ccs."ServiceID" ORDER BY acr."DateCreated" desc) AS "Amount",
           first_value(acr."DateCreated") OVER (partition by ccs."Code", ccs."ServiceID" ORDER BY acr."DateCreated" desc) AS "DateCreated"
           FROM
           "Customer_Contracts" ccs
           JOIN "Customer_ContractTrx" cctx ON (ccs."ContractID"=cctx."ContractID")
           JOIN "Acct_Revenue" acr ON cctx."ContractTrxID" = acr."ContractTrxID"
           WHERE
           ccs."ContractTypeCode" <>'NP'
           GROUP BY ccs."Code", ccs."ServiceID", acr."DateCreated")zp ON ccs."Code" = zp."Code" AND ccs."ServiceID" = zp."ServiceID"
           
--poslední provize NP
LEFT JOIN (SELECT ccs."Code", 
           ccs."ServiceID", 
           first_value(sum(acr."Amount")) OVER (partition by ccs."Code", ccs."ServiceID" ORDER BY acr."DateCreated" desc) AS "Amount",
           first_value(acr."DateCreated") OVER (partition by ccs."Code", ccs."ServiceID" ORDER BY acr."DateCreated" desc) AS "DateCreated"
           FROM
           "Customer_Contracts" ccs
           JOIN "Customer_ContractTrx" cctx ON (ccs."ContractID"=cctx."ContractID")
           JOIN "Acct_Revenue" acr ON cctx."ContractTrxID" = acr."ContractTrxID"
           WHERE
           ccs."ContractTypeCode" ='NP'
           GROUP BY ccs."Code", ccs."ServiceID",acr."DateCreated")np ON ccs."Code" = np."Code" AND ccs."ServiceID" = np."ServiceID"
           
WHERE 
ccs."ContractTypeCode" in ('NS','ZS')
AND cctx."ContractTrxStatusCode" NOT IN ('N','O','F')
AND ccat."ParentCategoryID"='10300'
;

-----------------------------------------------NEŽIVOT----------------------------------------------
INSERT INTO "active"
SELECT 
DISTINCT
ccs."Code",
bs."ServiceID",
			
CASE 
WHEN cctx."ContractTrxStatusCode" = 'ZO' AND np."Amount" IS NULL THEN 2
WHEN ccs."DateExpire"<$currentdate THEN 0
WHEN s1.BB=0 AND (np."Amount" IS NULL or np."Amount"<=0)  THEN 0
WHEN s1.BB=0 AND np."Amount">0 THEN 
                CASE WHEN bs."FollowUpMaximum" IS NULL THEN
                                                        CASE WHEN datediff(month,np."DateCreated", $currentdate) >=16 THEN 0 ELSE 1 END
                    WHEN bs."FollowUpMaximum" IS NOT NULL AND DATEADD(year,bs."FollowUpMaximum",ccs."DateEffective")<=$currentdate THEN 0
                    WHEN bs."FollowUpMaximum" IS NOT NULL AND DATEADD(year,bs."FollowUpMaximum",ccs."DateEffective")>=$currentdate THEN
                                                        CASE WHEN datediff(month,np."DateCreated", $currentdate) >=16 THEN 0 ELSE 1 END

                END
WHEN s1.BB<>0 AND np."Amount" <0 THEN 0
WHEN s1.BB<>0 AND bs."FollowUp"=1 THEN
                CASE WHEN bs."FollowUpMaximum" IS NULL THEN
                                                        CASE WHEN datediff(month,np."DateCreated", $currentdate) >=16 THEN 0 ELSE 1 END
                    WHEN bs."FollowUpMaximum" IS NOT NULL AND DATEADD(year,bs."FollowUpMaximum",ccs."DateEffective")<=$currentdate THEN 0
                    WHEN bs."FollowUpMaximum" IS NOT NULL AND DATEADD(year,bs."FollowUpMaximum",ccs."DateEffective")>=$currentdate THEN
                                                        CASE WHEN datediff(month,np."DateCreated", $currentdate) >=16 THEN 0 ELSE 1 END

                END
WHEN s1.BB <> 0 AND bs."FollowUp"=0 THEN 
                CASE WHEN ccs."DateExpire" >=$currentdate THEN 1 ELSE 0 END
END AS "Active"

FROM "Customer_Contracts" ccs
JOIN "Customer_ContractTrx" cctx ON (ccs."ContractID"=cctx."ContractID")
JOIN "Biz_Services" bs ON (ccs."ServiceID"=bs."ServiceID")
JOIN "Com_Categories" ccat ON (bs."CategoryID"=ccat."CategoryID")



--smlouvy které mají nulové BB - nejčastěji fake smlouvy které jsou vyčištěné, může jít i o nahrazované smlouvy
LEFT JOIN (SELECT sum(cctx."BusinessPoints") AS BB, ccs."Code", ccs."ServiceID" 
                FROM 
                "Customer_Contracts" ccs 
                JOIN "Customer_ContractTrx" cctx ON ccs."ContractID"=cctx."ContractID"
                WHERE cctx."ContractTrxStatusCode" NOT IN ('N','O','F')
                AND ccs."ContractTypeCode" <> 'NP'
                GROUP BY ccs."Code", ccs."ServiceID")s1 ON ccs."Code"=s1."Code" AND ccs."ServiceID"=s1."ServiceID"

--poslední provize k určení jestli je smlouva pořád aktivní nebo ne
LEFT JOIN (SELECT ccs."Code", 
           ccs."ServiceID", 
           first_value(sum(acr."Amount")) OVER (partition by ccs."Code", ccs."ServiceID" ORDER BY acr."DateCreated" desc) AS "Amount",
           first_value(acr."DateCreated") OVER (partition by ccs."Code", ccs."ServiceID" ORDER BY acr."DateCreated" desc) AS "DateCreated"
           FROM
           "Customer_Contracts" ccs
           JOIN "Customer_ContractTrx" cctx ON (ccs."ContractID"=cctx."ContractID")
           JOIN "Acct_Revenue" acr ON cctx."ContractTrxID" = acr."ContractTrxID"
           GROUP BY ccs."Code", ccs."ServiceID", acr."DateCreated")np ON ccs."Code" = np."Code" AND ccs."ServiceID" = np."ServiceID"
           
WHERE 
ccs."ContractTypeCode" in ('NS','ZS')
AND cctx."ContractTrxStatusCode" NOT IN ('N','O','F')
AND (s1.BB=0 or np."Amount" IS NOT NULL)
AND ccat."ParentCategoryID"='10400'
--AND ccs.ClientID NOT IN (200139307,200170142) -- pro CZ
;

-----------------------------------------------INVESTICE----------------------------------------------

INSERT INTO "active"

SELECT 
DISTINCT
ccs."Code",
bs."ServiceID",
CASE WHEN cctx."ContractTrxStatusCode" = 'ZO' AND zp."Amount" IS NULL THEN 2
	 WHEN s1.BB IS NULL THEN 0
	 WHEN s1.BB = 0 AND zp."Amount" IS NULL THEN 0
	 WHEN s1.BB = 0 AND zp."Amount" IS NOT NULL THEN
		CASE WHEN zp."Amount" <= 0 THEN 0
			 WHEN zp."Amount" > 0 THEN 1
		END
	 WHEN s1.BB > 0 THEN
		CASE WHEN zp."Amount" > 0 THEN 1
			 WHEN zp."Amount" < 0 THEN 0
			 WHEN zp."Amount" IS NULL AND cctx."BusinessPeriodID"+12<$bp THEN 0
			 WHEN zp."Amount" IS NULL AND cctx."BusinessPeriodID"+12>=$bp THEN 1
			 WHEN zp."Amount" = 0 THEN 1 
		END
	 END AS "Active"

FROM "Customer_Contracts" ccs
JOIN "Customer_ContractTrx" cctx ON (ccs."ContractID"=cctx."ContractID")
JOIN "Biz_Services" bs ON (ccs."ServiceID"=bs."ServiceID")
JOIN "Com_Categories" ccat ON (bs."CategoryID"=ccat."CategoryID")



--smlouvy které mají nulové BB - nejčastěji fake smlouvy které jsou vyčištěné, může jít i o nahrazované smlouvy
LEFT JOIN (SELECT sum(cctx."BusinessPoints") AS BB, ccs."Code", ccs."ServiceID" 
                FROM 
                "Customer_Contracts" ccs 
                JOIN "Customer_ContractTrx" cctx ON ccs."ContractID"=cctx."ContractID"
                WHERE cctx."ContractTrxStatusCode" NOT IN ('N','O','F')
                AND ccs."ContractTypeCode" <> 'NP'
                GROUP BY ccs."Code", ccs."ServiceID")s1 ON ccs."Code"=s1."Code" AND ccs."ServiceID"=s1."ServiceID"

--poslední provize (ne NP)
LEFT JOIN (SELECT ccs."Code", 
           ccs."ServiceID", 
           first_value(sum(acr."Amount")) OVER (partition by ccs."Code", ccs."ServiceID" ORDER BY acr."DateCreated" desc) AS "Amount",
           first_value(acr."DateCreated") OVER (partition by ccs."Code", ccs."ServiceID" ORDER BY acr."DateCreated" desc) AS "DateCreated"
           FROM
           "Customer_Contracts" ccs
           JOIN "Customer_ContractTrx" cctx ON (ccs."ContractID"=cctx."ContractID")
           JOIN "Acct_Revenue" acr ON cctx."ContractTrxID" = acr."ContractTrxID"
           WHERE
           ccs."ContractTypeCode" <>'NP'
           GROUP BY ccs."Code", ccs."ServiceID", acr."DateCreated")zp ON ccs."Code" = zp."Code" AND ccs."ServiceID" = zp."ServiceID"
           


WHERE
ccs."ContractTypeCode" in ('NS','ZS')
AND cctx."ContractTrxStatusCode" NOT IN ('N','O','F')
AND ccat."ParentCategoryID"='10500'
AND ccat."CategoryID" NOT IN (100077)
AND bs."ServiceID" NOT IN ('510059976','510060104','510060465','510060497','510060498','510060499','510060500','510060612','510060636','510060761','510060772','510060808') --pro CZ
--AND ccs.ClientID NOT IN (200139307,200170142) -- pro CZ

;

-----------------------------------------------ÚVĚRY----------------------------------------------

INSERT INTO "active"

SELECT 
DISTINCT 
ccs."Code", 
bs."ServiceID",
CASE WHEN cctx."ContractTrxStatusCode" = 'ZO' AND zp."Amount" IS NULL THEN 2
	 WHEN s1.BB IS NULL THEN 0

	 WHEN s1.BB = 0 AND zp."Amount" IS NULL THEN
		CASE WHEN s2."Text" IS NULL THEN 0
			 WHEN s2."Text" IS NOT NULL THEN 1
		END

	 WHEN s1.BB = 0 AND zp."Amount" IS NOT NULL THEN
		CASE WHEN zp."Amount" <= 0 THEN 0
			 WHEN zp."Amount" > 0 THEN 1
		END

	 WHEN s1.BB > 0 THEN
		CASE WHEN zp."Amount" > 0 THEN 1
			 WHEN zp."Amount" < 0 THEN 0
			 WHEN zp."Amount" IS NULL AND cctx."BusinessPeriodID"+12<$bp THEN 0
			 WHEN zp."Amount" IS NULL AND cctx."BusinessPeriodID"+12>=$bp THEN 1
			 WHEN zp."Amount" = 0 THEN 1 --tady je to ještě diskutabilní, protože někdy tam může být poslední provize záporná
				
		END

	 END AS aktivni
	 	 
--když jsou BB nenulové, provize null a poznámka existuje, tak je to přeúčtovaná smlouva a tu brát jako aktivní

FROM "Customer_Contracts" ccs
JOIN "Customer_ContractTrx" cctx ON (ccs."ContractID"=cctx."ContractID")
JOIN "Biz_Services" bs ON (ccs."ServiceID"=bs."ServiceID")
JOIN "Com_Categories" ccat ON (bs."CategoryID"=ccat."CategoryID")

--smlouvy které mají nulové BB - nejčastěji fake smlouvy které jsou vyčištěné, může jít i o nahrazované smlouvy
LEFT JOIN (SELECT sum(cctx."BusinessPoints") AS BB, ccs."Code", ccs."ServiceID" 
                FROM 
                "Customer_Contracts" ccs 
                JOIN "Customer_ContractTrx" cctx ON ccs."ContractID"=cctx."ContractID"
                WHERE cctx."ContractTrxStatusCode" NOT IN ('N','O','F')
                AND ccs."ContractTypeCode" <> 'NP'
                GROUP BY ccs."Code", ccs."ServiceID")s1 ON ccs."Code"=s1."Code" AND ccs."ServiceID"=s1."ServiceID"


--poslední provize (ne NP)
LEFT JOIN (SELECT ccs."Code", 
           ccs."ServiceID", 
           first_value(sum(acr."Amount")) OVER (partition by ccs."Code", ccs."ServiceID" ORDER BY acr."DateCreated" desc) AS "Amount",
           first_value(acr."DateCreated") OVER (partition by ccs."Code", ccs."ServiceID" ORDER BY acr."DateCreated" desc) AS "DateCreated"
           FROM
           "Customer_Contracts" ccs
           JOIN "Customer_ContractTrx" cctx ON (ccs."ContractID"=cctx."ContractID")
           JOIN "Acct_Revenue" acr ON cctx."ContractTrxID" = acr."ContractTrxID"
           WHERE ccs."ContractTypeCode" <>'NP'
           GROUP BY ccs."Code", ccs."ServiceID", acr."DateCreated")zp ON ccs."Code" = zp."Code" AND ccs."ServiceID" = zp."ServiceID"
          

--textové poznámky v případě že dochází k přeúčtování provize
LEFT JOIN (SELECT cctn."ContractTrxID", cctn."Text" FROM "Customer_ContractTrxNotes" cctn
				WHERE cctn."Text" LIKE 'přeúčtov%')s2 ON cctx."ContractTrxID"=s2."ContractTrxID"


WHERE 
ccat."ParentCategoryID" = '10200'
AND ccs."ContractTypeCode" in ('NS','ZS')
--AND ccs.clientid NOT IN (200139307,200170142) -- pro CZ
;

-----------------------------------------------SPOŘENÍ----------------------------------------------

INSERT INTO "active"

SELECT 
DISTINCT 
ccs."Code", 
bs."ServiceID",
CASE WHEN cctx."ContractTrxStatusCode" = 'ZO' AND zp."Amount" IS NULL THEN 2
	 WHEN s1.BB IS NULL THEN 0

	 WHEN s1.BB = 0 AND zp."Amount" IS NULL THEN
		CASE WHEN s2."Text" IS NULL THEN 0
			 WHEN s2."Text" IS NOT NULL THEN 1
		END

	 WHEN s1.BB = 0 AND zp."Amount" IS NOT NULL THEN
		CASE WHEN zp."Amount" <= 0 THEN 0
			 WHEN zp."Amount" > 0 THEN 1
		END

	 WHEN s1.BB > 0 THEN
		CASE WHEN zp."Amount" > 0 THEN 1
			 WHEN zp."Amount" < 0 THEN 0
			 WHEN zp."Amount" IS NULL AND cctx."BusinessPeriodID"+12<$bp THEN 0
			 WHEN zp."Amount" IS NULL AND cctx."BusinessPeriodID"+12>=$bp THEN 1
			 WHEN zp."Amount" = 0 THEN 1 --tady je to ještě diskutabilní, protože někdy tam může být poslední provize záporná
				
		END

	 END AS "Active"



FROM "Customer_Contracts" ccs
JOIN "Customer_ContractTrx" cctx ON (ccs."ContractID"=cctx."ContractID")
JOIN "Biz_Services" bs ON (ccs."ServiceID"=bs."ServiceID")
JOIN "Com_Categories" ccat ON (bs."CategoryID"=ccat."CategoryID")

--smlouvy které mají nulové BB - nejčastěji fake smlouvy které jsou vyčištěné, může jít i o nahrazované smlouvy
LEFT JOIN (SELECT sum(cctx."BusinessPoints") AS BB, ccs."Code", ccs."ServiceID" 
                FROM 
                "Customer_Contracts" ccs 
                JOIN "Customer_ContractTrx" cctx ON ccs."ContractID"=cctx."ContractID"
                WHERE cctx."ContractTrxStatusCode" NOT IN ('N','O','F')
                AND ccs."ContractTypeCode" <> 'NP'
                GROUP BY ccs."Code", ccs."ServiceID")s1 ON ccs."Code"=s1."Code" AND ccs."ServiceID"=s1."ServiceID"


--poslední provize (ne NP)
LEFT JOIN (SELECT ccs."Code", 
           ccs."ServiceID", 
           first_value(sum(acr."Amount")) OVER (partition by ccs."Code", ccs."ServiceID" ORDER BY acr."DateCreated" desc) AS "Amount",
           first_value(acr."DateCreated") OVER (partition by ccs."Code", ccs."ServiceID" ORDER BY acr."DateCreated" desc) AS "DateCreated"
           FROM
           "Customer_Contracts" ccs
           JOIN "Customer_ContractTrx" cctx ON (ccs."ContractID"=cctx."ContractID")
           JOIN "Acct_Revenue" acr ON cctx."ContractTrxID" = acr."ContractTrxID"
           WHERE
           ccs."ContractTypeCode" <>'NP'
           GROUP BY ccs."Code", ccs."ServiceID", acr."DateCreated")zp ON ccs."Code" = zp."Code" AND ccs."ServiceID" = zp."ServiceID"
          

--textové poznámky v případě že dochází k přeúčtování provize
LEFT JOIN (SELECT cctn."ContractTrxID", cctn."Text" FROM "Customer_ContractTrxNotes" cctn
				WHERE cctn."Text" LIKE 'přeúčtov%')s2 ON cctx."ContractTrxID"=s2."ContractTrxID"



WHERE 
ccs."ContractTypeCode" in ('NS','ZS')
AND ccat."ParentCategoryID" in ('10100')
;

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-------------------------------AGENTS TO CLIENTS-----------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
--if there is different agent to some family members, family has to be split!!!!
--RMagent from last contract is to be assigned to client
--only family members with the same RMagent can be part of one family
--so it may happen, that in when clients in reality are from same family
--in our case they will be separated
CREATE TABLE "client_rmagent" AS
SELECT "ClientID", "RMAgentID" FROM (
SELECT DISTINCT ccs."ClientID", 
ccs."DateSigned", 
ccs."RMAgentID", 
RANK() OVER(PARTITION BY ccs."ClientID" ORDER BY ccs."DateSigned" DESC, ccs."RMAgentID" DESC) AS "rnk"
FROM "Customer_Contracts" ccs
)sub
WHERE sub."rnk" = 1
;

--select count(*) from "client_rmagent"; 
--select count(distinct "ClientID") from "client_rmagent";






-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------START OF FAMILY STUFF---------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
--first create oldies table which contains only those relations, WHERE main
--client is older THEN related client
CREATE TABLE "oldies" AS 
SELECT cpr."PartyID", 
cc."ClientID",
cc."BirthDate"
FROM "Com_PartyRelationships" cpr 
JOIN "Customer_Clients" cc ON cpr."PartyID" = cc."PartyID"
JOIN "client_rmagent" cr on cc."ClientID" = cr."ClientID"
JOIN "Customer_Clients" cc2 ON cpr."RelatedPartyID" = cc2."PartyID"
JOIN "client_rmagent" cr2 on cc2."ClientID" = cr2."ClientID"
WHERE cpr."RelatedPartyTypeCode" in ('F','CH','P','W','H')
AND cc."BirthDate" < cc2."BirthDate"
AND cr."RMAgentID" = cr2."RMAgentID"
;

--THEN for oldies check, whether exists someone who is older

CREATE TABLE "oldies2" as
SELECT ol."PartyID"
FROM "oldies" ol
JOIN "client_rmagent" cr on ol."ClientID" = cr."ClientID"
JOIN "Com_PartyRelationships" cpr ON ol."PartyID" = cpr."PartyID"
JOIN "Customer_Clients" cc ON cpr."RelatedPartyID" = cc."PartyID"
JOIN "client_rmagent" cr2 on cc."ClientID" = cr2."ClientID"
WHERE cc."BirthDate" < ol."BirthDate"
AND cr."RMAgentID" = cr2."RMAgentID"
;

--delete them FROM oldies table
DELETE FROM "oldies" WHERE "PartyID" in (SELECT "PartyID" FROM "oldies2");

--create family table which contains PartyID (AS PK) of head of the family 
--(FROM oldies table) AND clientID's of all family members
CREATE TABLE "families" as
SELECT DISTINCT "PartyID" AS "FamilyID", 
"ClientID",
1 AS "IsHead"
FROM "oldies";

INSERT INTO "families"
SELECT DISTINCT ol."PartyID" AS "FamilyID", 
cc."ClientID",
0 AS "IsHead"
FROM "oldies" ol
JOIN "Com_PartyRelationships" cpr ON ol."PartyID" = cpr."RelatedPartyID"
JOIN "client_rmagent" cr on ol."ClientID" = cr."ClientID"
JOIN "Customer_Clients" cc ON cpr."PartyID" = cc."PartyID"
JOIN "client_rmagent" cr2 on cc."ClientID" = cr2."ClientID"
WHERE cpr."RelatedPartyTypeCode" in ('F','CH','P','W','H')
AND cr."RMAgentID" = cr2."RMAgentID"
;

--insert rest of the clients who does not have any relations
INSERT INTO "families"
SELECT  cc."PartyID", cc."ClientID",
1 AS "IsHead"
FROM "Customer_Clients" cc
WHERE 
cc."ClientID" NOT IN (SELECT "ClientID" FROM "families");

--Delete FROM "families" WHERE "ClientID" NOT IN (SELECT "ClientID" FROM "Customer_Contracts");

---------------------------------------------------------------
---------------------------------------------------------------
----------------------------CARE AGENTS------------------------
---------------------------------------------------------------
---------------------------------------------------------------
--table contractagents keeps for each customer's last contract all relevant agents
--and gives them rank values, based on their type and percentshare on given contract
/*
create table "contractagents"
as
select ccs."ClientID", 
ccs."ContractID", 
ccs."DateSigned", 
cctx."ContractTrxID", 
cctx."DateCreated",
ccta."PercentShare", 
aa."AgentID", 
aa."AgentStatus",
case when ccta."PercentShare" > 50 then 2
when ccta."PercentShare" = 50 and ccta."ContractTrxAgentTypeID" = 4 then 3
when ccta."PercentShare" = 50 and ccta."ContractTrxAgentTypeID" <> 4 then 4
when ccta."PercentShare" <50 and ccta."ContractTrxAgentTypeID" = 4 then 5
else 6 end as "rank"
from 
"Customer_Clients" cc
join (select distinct "ClientID",
first_value("ContractID") over (partition by "ClientID" order by "DateSigned" desc, "DateUpdated" desc) "ContractID"
from "Customer_Contracts")con on cc."ClientID" = con."ClientID"
join "Customer_Contracts" ccs on con."ContractID" = ccs."ContractID"
join "Customer_ContractTrx" cctx on ccs."ContractID" = cctx."ContractID"
join "Customer_ContractTrxAgents" ccta on cctx."ContractTrxID" = ccta."ContractTrxID"
join "Agent_Agents" aa on ccta."AgentID" = aa."AgentID"
where 
(ccta."PercentShare">=50 or ccta."ContractTrxAgentTypeID" = 4)
and aa."AgentID" not in ('400000200','400002648','400000597','400149971','400010964','400012231','400012198','400151825')
;

--this only inserts additional data to contractagents table 
--RM agent (contract managet) is inserted and has highest rank
insert into "contractagents"
select  ccs."ClientID", 
ccs."ContractID", 
ccs."DateSigned", 
cctx."ContractTrxID", 
cctx."DateCreated",
100 as "PercentShare", 
aa."AgentID", 
aa."AgentStatus",
1 as "Rank"
from 
"Customer_Clients" cc
join (select distinct "ClientID",
first_value("ContractID") over (partition by "ClientID" order by "DateSigned" desc, "DateUpdated" desc) "ContractID"
from "Customer_Contracts")con on cc."ClientID" = con."ClientID"
join "Customer_Contracts" ccs on con."ContractID" = ccs."ContractID"
join "Customer_ContractTrx" cctx on ccs."ContractID" = cctx."ContractID"
join "Agent_Agents" aa on ccs."RMAgentID" = aa."AgentID"
where 
aa."AgentID" not in ('400000200','400002648','400000597','400149971','400010964','400012231','400012198','400151825')
;

--table care adds additional information about agents ranks and
--ads family ID, as it is important for desired sorting
create table "care"
as
select distinct cc."ClientID", 
ca."ContractTrxID",
ca."DateCreated",
ca."DateSigned",
cc."BirthDate", 
ca."AgentID",
aa."AgentStatus",
ca."rank",
case when ca."rank" = 1 then 'přiřazený poradce je správcem smlouvy'
when ca."rank" = 2 then 'přiřazený poradce má majoritní podíl'
when ca."rank" = 3 then 'přiřazený poradce je realizátor s 50% podílem'
when ca."rank" = 4 then 'přiřazený poradce není realizátor a má 50% podíl'
when ca."rank" = 5 then 'přiřazený poradce je realizátor s podílem <50%'
when ca."rank" = 6 then 'přiřazený poradce má nenulový podíl na smlouvě'
when ca."rank" is null then 'přiřazený poradce je neaktivní správce smlouvy'
end as "Message",
cc2."ClientID" as "FamilyHeadClientID"
from "Customer_Clients" cc
left  join "contractagents" ca on cc."ClientID" = ca."ClientID"
left  join "Agent_Agents" aa on ca."AgentID" = aa."AgentID"
join "families" f on cc."ClientID" = f."ClientID"
join "Customer_Clients" cc2 on f."FamilyID" = cc2."PartyID"
;

--clientagents is final table which contains clientid, familyid, careagentid.
--Logic  is following: select first active agent from family head's list of
--contractagents. If no active agents is alive, go on second oldest family member
--and choose among his contractagents and so on. If no active agents is found, first
--inactive agents is joined - again following rule that family head's(oldest member)
--care agent will be selected
--This is not perfect, as aproximately 30k clients does not have select care agent, but
--they are mainly historical agents and so far it doesnt matter.
create table "clientagents"
as
select cc."ClientID", 
f."FamilyID", 
ifnull(s1."AgentID",s2."AgentID") as "AgentID"

from "Customer_Clients" cc
join  "families" f on cc."ClientID" = f."ClientID"
join "Customer_Clients" cc2 on f."FamilyID" = cc2."PartyID"

left join (select distinct c."ClientID",
first_value(c."AgentID") over (partition by c."ClientID" order by c."rank") "AgentID"
from "care" c
where c."AgentStatus" = 'A')s1 on cc2."ClientID" = s1."ClientID"

left join (select distinct f."FamilyID",
first_value(c."AgentID") over (partition by f."FamilyID" order by c."rank") "AgentID"
from "care" c
join "families" f on c."ClientID" = f."ClientID"            
)s2 on f."FamilyID" = s2."FamilyID"
;
*/
---------------------------------------------------
---------------------------------------------------
---------------FINISH FAMILY STUFF-----------------
---------------------------------------------------
---------------------------------------------------
--CCreate final table of families -> FamilyClients
CREATE TABLE "FamilyClients" AS
SELECT f."FamilyID",
f."ClientID",
current_date AS "DateCreated",
NULL as "CreatedBy",
current_date AS "DateUpdated",
NULL as "UpdatedBy",
cr."RMAgentID" as "AgentID",
1 as "Active",
NULL as "Note",
f."IsHead"
FROM "families" f
JOIN "client_rmagent" cr on f."ClientID" = cr."ClientID"
;


--select count(distinct "FamilyID") from "FamilyClients";
--select count(*) from "FamilyClients";
--select "cnt", count(*) from (select "FamilyID", count(distinct "AgentID") as "cnt" from "FamilyClients" group by "FamilyID")sub group by "cnt";

--------------------------------------------------------
--------------------------------------------------------
---------------------Client degrees---------------------
--------------------------------------------------------
--------------------------------------------------------


--ING
CREATE TABLE "tmp_degrees" ("ClientID" varchar(100), "Degree" varchar(100)) AS 
SELECT cc."ClientID",
CASE WHEN LOWER(cp."Prefix") LIKE '%ing%' or LOWER(cp."Suffix") LIKE '%ing%' THEN 'Ing' ELSE 'NA' END AS "Degree"

FROM "Customer_Clients" cc
JOIN "Com_Parties" cp ON cc."PartyID" = cp."PartyID"
WHERE CASE WHEN LOWER(cp."Prefix") LIKE '%ing%' or LOWER(cp."Suffix") LIKE '%ing%' THEN 'Ing' ELSE 'NA' END  = 'Ing'
;

--MGR
INSERT INTO "tmp_degrees"
SELECT cc."ClientID",
CASE WHEN LOWER(cp."Prefix") LIKE '%mgr%' or LOWER(cp."Suffix") LIKE '%mgr%' THEN 'Mgr' ELSE 'NA' END AS "Degree"

FROM "Customer_Clients" cc
JOIN "Com_Parties" cp ON cc."PartyID" = cp."PartyID"
WHERE CASE WHEN LOWER(cp."Prefix") LIKE '%mgr%' or LOWER(cp."Suffix") LIKE '%mgr%' THEN 'Mgr' ELSE 'NA' END  = 'Mgr'
;

--MUDR
INSERT INTO "tmp_degrees"
SELECT cc."ClientID",
CASE WHEN LOWER(cp."Prefix") LIKE '%mudr%' or LOWER(cp."Suffix") LIKE '%mudr%' THEN 'MUDR' ELSE 'NA' END AS "Degree"

FROM "Customer_Clients" cc
JOIN "Com_Parties" cp ON cc."PartyID" = cp."PartyID"
WHERE CASE WHEN LOWER(cp."Prefix") LIKE '%mudr%' or LOWER(cp."Suffix") LIKE '%mudr%' THEN 'MUDR' ELSE 'NA' END  = 'MUDR'
;

--JUDR
INSERT INTO "tmp_degrees"
SELECT cc."ClientID",
CASE WHEN LOWER(cp."Prefix") LIKE '%judr%' or LOWER(cp."Suffix") LIKE '%judr%' THEN 'JUDR' ELSE 'NA' END AS "Degree"

FROM "Customer_Clients" cc
JOIN "Com_Parties" cp ON cc."PartyID" = cp."PartyID"
WHERE CASE WHEN LOWER(cp."Prefix") LIKE '%judr%' or LOWER(cp."Suffix") LIKE '%judr%' THEN 'JUDR' ELSE 'NA' END  = 'JUDR'
;

--MBA
INSERT INTO "tmp_degrees"
SELECT cc."ClientID",
CASE WHEN LOWER(cp."Prefix") LIKE '%mba%' or LOWER(cp."Suffix") LIKE '%mba%' THEN 'MBA' ELSE 'NA' END AS "Degree"

FROM "Customer_Clients" cc
JOIN "Com_Parties" cp ON cc."PartyID" = cp."PartyID"
WHERE CASE WHEN LOWER(cp."Prefix") LIKE '%mba%' or LOWER(cp."Suffix") LIKE '%mba%' THEN 'MBA' ELSE 'NA' END  = 'MBA'
;

--Bc
INSERT INTO "tmp_degrees"
SELECT cc."ClientID",
CASE WHEN LOWER(cp."Prefix") LIKE '%bc%' or LOWER(cp."Suffix") LIKE '%bc%' THEN 'Bc' ELSE 'NA' END AS "Degree"

FROM "Customer_Clients" cc
JOIN "Com_Parties" cp ON cc."PartyID" = cp."PartyID"
WHERE CASE WHEN LOWER(cp."Prefix") LIKE '%bc%' or LOWER(cp."Suffix") LIKE '%bc%' THEN 'Bc' ELSE 'NA' END  = 'Bc'
;

--PhDr
INSERT INTO "tmp_degrees"
SELECT cc."ClientID",
CASE WHEN LOWER(cp."Prefix") LIKE '%phdr%' or LOWER(cp."Suffix") LIKE '%phdr%' THEN 'PhDr' ELSE 'NA' END AS "Degree"

FROM "Customer_Clients" cc
JOIN "Com_Parties" cp ON cc."PartyID" = cp."PartyID"
WHERE CASE WHEN LOWER(cp."Prefix") LIKE '%phdr%' or LOWER(cp."Suffix") LIKE '%phdr%' THEN 'PhDr' ELSE 'NA' END  = 'PhDr'
;

--RNDr
INSERT INTO "tmp_degrees"
SELECT cc."ClientID",
CASE WHEN LOWER(cp."Prefix") LIKE '%rndr%' or LOWER(cp."Suffix") LIKE '%rndr%' THEN 'RNDr' ELSE 'NA' END AS "Degree"

FROM "Customer_Clients" cc
JOIN "Com_Parties" cp ON cc."PartyID" = cp."PartyID"
WHERE CASE WHEN LOWER(cp."Prefix") LIKE '%rndr%' or LOWER(cp."Suffix") LIKE '%rndr%' THEN 'RNDr' ELSE 'NA' END  = 'RNDr'
;

--Arch
INSERT INTO "tmp_degrees"
SELECT cc."ClientID",
CASE WHEN LOWER(cp."Prefix") LIKE '%arch%' or LOWER(cp."Suffix") LIKE '%arch%' THEN 'Arch' ELSE 'NA' END AS "Degree"

FROM "Customer_Clients" cc
JOIN "Com_Parties" cp ON cc."PartyID" = cp."PartyID"
WHERE CASE WHEN LOWER(cp."Prefix") LIKE '%arch%' or LOWER(cp."Suffix") LIKE '%arch%' THEN 'Arch' ELSE 'NA' END  = 'Arch'
;

--SELECT * FROM "tmp_degrees";

--------Degree rank--------------
CREATE TABLE "degrees" AS 
SELECT sub1."Degree", AVG(sub1."Amount") AS "Amount",
RANK() OVER (ORDER BY AVG(sub1."Amount") desc) AS "Rank2"
FROM(
SELECT td."ClientID", td."Degree", sum(acr."Amount") AS "Amount" 
FROM "tmp_degrees" td
JOIN "Customer_Contracts" ccs ON td."ClientID" = ccs."ClientID"
JOIN "Customer_ContractTrx" cctx ON ccs."ContractID" = cctx."ContractID"
JOIN "Acct_Revenue" acr ON cctx."ContractTrxID" = acr."ContractTrxID"
GROUP BY td."ClientID", td."Degree")sub1
GROUP BY sub1."Degree"
;

------------------------------------------------------------------
------------------------------------------------------------------
--------------------------Client JOBS-----------------------------
------------------------------------------------------------------
------------------------------------------------------------------
--first table prepares data so that we have unified job names
--clients without unified job name wont be counted
CREATE TABLE "clientjobs" as
SELECT cca."ClientID",
CASE WHEN  LOWER(cca."StringValue") LIKE 'adm.prac%' THEN 'administrativa'
 WHEN LOWER(cca."StringValue") LIKE 'admin.prac%' THEN 'administrativa'
 WHEN LOWER(cca."StringValue") LIKE 'admin. prac%' THEN 'administrativa'
 WHEN LOWER(cca."StringValue") LIKE 'admin prac%' THEN 'administrativa'
 WHEN LOWER(cca."StringValue") LIKE 'administr.%' THEN 'administrativa'
 WHEN LOWER(cca."StringValue") LIKE 'administ%' THEN 'administrativa'
 WHEN LOWER(cca."StringValue") LIKE '%administrat%' THEN 'administrativa'
 WHEN LOWER(cca."StringValue") LIKE '%administat%' THEN 'administrativa'
 WHEN LOWER(cca."StringValue") LIKE '%administrace%' THEN 'administrativa'
 WHEN LOWER(cca."StringValue") LIKE 'admin.%' THEN 'administrativa'
 WHEN LOWER(cca."StringValue") LIKE 'adm.%' THEN 'administrativa'
 WHEN LOWER(cca."StringValue") LIKE '%administrátor%' THEN 'administrátor'
 WHEN LOWER(cca."StringValue") LIKE '%advok%' THEN 'advokát'
 WHEN LOWER(cca."StringValue") LIKE '%agronom%' THEN 'zemědělský inženýr, agronom'
 WHEN LOWER(cca."StringValue") LIKE '%analyti%' THEN 'analytik'
 WHEN LOWER(cca."StringValue") LIKE '%architek%' THEN 'architekt'
 WHEN LOWER(cca."StringValue") LIKE '%archiv%' THEN 'archivář, správce rejstříku'
 WHEN LOWER(cca."StringValue") LIKE '%asisten%' THEN 'asistent'
 WHEN LOWER(cca."StringValue") LIKE '%sekretářka%' THEN 'asistent'
 WHEN LOWER(cca."StringValue") LIKE '%audito%' THEN 'auditor'
 WHEN LOWER(cca."StringValue") LIKE '%autodoprav%' THEN 'autodoprava'
 WHEN LOWER(cca."StringValue") LIKE 'automech%' THEN 'automechanik'
 WHEN LOWER(cca."StringValue") LIKE 'autoserv%' THEN 'automechanik'
 WHEN LOWER(cca."StringValue") LIKE '%bagr%' THEN 'operátor stavebních strojů'
 WHEN LOWER(cca."StringValue") LIKE '%balič%' THEN 'balič'
 WHEN LOWER(cca."StringValue") LIKE '%bankéř%' THEN 'bankéř'
 WHEN LOWER(cca."StringValue") LIKE '%bank.%' THEN 'banka'
 WHEN LOWER(cca."StringValue") LIKE '%bankovní%' THEN 'banka'
 WHEN LOWER(cca."StringValue") LIKE '%barman%' THEN 'barman'
 WHEN LOWER(cca."StringValue") LIKE '%brusič%' THEN 'brusič'
 WHEN LOWER(cca."StringValue") LIKE '%celní%' THEN 'celník'
 WHEN LOWER(cca."StringValue") LIKE '%cnc%' THEN 'CNC obsluha'
 WHEN LOWER(cca."StringValue") LIKE '%cukr%' THEN 'cukrář'
 WHEN LOWER(cca."StringValue") LIKE '%čaloun%' THEN 'čalouník'
 WHEN LOWER(cca."StringValue") LIKE '%číšn%' THEN 'číšník'
 WHEN LOWER(cca."StringValue") LIKE '%čišn%' THEN 'číšník'
 WHEN LOWER(cca."StringValue") LIKE '%dělní%' THEN 'dělník'
 WHEN LOWER(cca."StringValue") LIKE '%dělni%' THEN 'dělník'
 WHEN LOWER(cca."StringValue") LIKE '%dispeč%' THEN 'dispečer'
 WHEN LOWER(cca."StringValue") LIKE '%dítě%' THEN 'dítě'
 WHEN LOWER(cca."StringValue") LIKE '%důchod%' THEN 'důchodce'
 WHEN LOWER(cca."StringValue") LIKE '%důch.' THEN 'důchodce'
 WHEN LOWER(cca."StringValue") LIKE '%ekonom%' THEN 'ekonom'
 WHEN LOWER(cca."StringValue") LIKE '%elektri%' THEN 'elektrikář'
 WHEN LOWER(cca."StringValue") LIKE '%elektromechan%' THEN 'elektrikář'
 WHEN LOWER(cca."StringValue") LIKE '%elektrotechnik%' THEN 'elektrikář'
 WHEN LOWER(cca."StringValue") LIKE '%faktur%' THEN 'fakturant/ka'
 WHEN LOWER(cca."StringValue") LIKE 'fin.porad%' THEN 'finanční_poradce'
 WHEN LOWER(cca."StringValue") LIKE 'finanční porad%' THEN 'finanční_poradce'
 WHEN LOWER(cca."StringValue") LIKE 'fin. porad%' THEN 'finanční_poradce'
 WHEN LOWER(cca."StringValue") LIKE 'finančník%' THEN 'finančník'
 WHEN LOWER(cca."StringValue") LIKE '%fotbalist%' THEN 'sportovec'
 WHEN LOWER(cca."StringValue") LIKE '%fotograf%' THEN 'fotograf'
 WHEN LOWER(cca."StringValue") LIKE '%fréza%' THEN 'frézař'
 WHEN LOWER(cca."StringValue") LIKE '%fyzioterap%' THEN 'fyzioterapeut/ka'
 WHEN LOWER(cca."StringValue") LIKE '%geodet%' THEN 'geodet/ka'
 WHEN LOWER(cca."StringValue") LIKE '%geodét%' THEN 'geodet/ka'
 WHEN LOWER(cca."StringValue") LIKE '%grafi%' THEN 'grafik'
 WHEN LOWER(cca."StringValue") LIKE '%hasič%' THEN 'hasič'
 WHEN LOWER(cca."StringValue") LIKE '%herec%' THEN 'herec/čka'
 WHEN LOWER(cca."StringValue") LIKE '%hereč%' THEN 'herec/čka'
 WHEN LOWER(cca."StringValue") LIKE '%hlídač%' THEN 'hlídač'
 WHEN LOWER(cca."StringValue") LIKE '%hlídac%' THEN 'hlídač'
 WHEN LOWER(cca."StringValue") LIKE '%hokejist%' THEN 'sportovec'
 WHEN LOWER(cca."StringValue") LIKE '%horník%' THEN 'horník'
 WHEN LOWER(cca."StringValue") LIKE '%hudeb%' THEN 'hudebník'
 WHEN LOWER(cca."StringValue") LIKE '%chemi%' THEN 'chemik'
 WHEN LOWER(cca."StringValue") LIKE 'chemik' THEN 'chemik'
 WHEN LOWER(cca."StringValue") LIKE '%instal%' THEN 'instalatér'
 WHEN LOWER(cca."StringValue") LIKE '%inžen%' THEN 'inženýr'
 WHEN LOWER(cca."StringValue") LIKE 'it' THEN 'IT'
 WHEN LOWER(cca."StringValue") LIKE '%it tech%' THEN 'IT'
 WHEN LOWER(cca."StringValue") LIKE '%it spec%' THEN 'IT'
 WHEN LOWER(cca."StringValue") LIKE '% it' THEN 'IT'
 WHEN LOWER(cca."StringValue") LIKE 'it prac%' THEN 'IT'
 WHEN LOWER(cca."StringValue") LIKE 'informatik' THEN 'IT'
 WHEN LOWER(cca."StringValue") LIKE '%it konz%' THEN 'IT'
 WHEN LOWER(cca."StringValue") LIKE '%informační%' THEN 'IT'
 WHEN LOWER(cca."StringValue") LIKE '%jednat%' THEN 'jednatel'
 WHEN LOWER(cca."StringValue") LIKE '%jeřáb%' THEN 'jeřábník'
 WHEN LOWER(cca."StringValue") LIKE '%kadeř%' THEN 'kadeřník'
 WHEN LOWER(cca."StringValue") LIKE '%klempíř%' THEN 'klempíř'
 WHEN LOWER(cca."StringValue") LIKE '%knihov%' THEN 'knihovník/ce'
 WHEN LOWER(cca."StringValue") LIKE 'konstrukt%' THEN 'konstruktér'
 WHEN LOWER(cca."StringValue") LIKE '%kontrolor%' THEN 'kontrolor'
 WHEN LOWER(cca."StringValue") LIKE '%kosmetička%' THEN 'kosmetička'
 WHEN LOWER(cca."StringValue") LIKE '%kovář%' THEN 'kovář'
 WHEN LOWER(cca."StringValue") LIKE '%kucha%' THEN 'kuchař/ka'
 WHEN LOWER(cca."StringValue") LIKE '%laborant%' THEN 'laborant/ka'
 WHEN LOWER(cca."StringValue") LIKE '%lakýrník%' THEN 'lakýrník'
 WHEN LOWER(cca."StringValue") LIKE '%lékárn%' THEN 'lékárník/ce'
 WHEN LOWER(cca."StringValue") LIKE '%farmace%' THEN 'lékárník/ce'
 WHEN LOWER(cca."StringValue") LIKE '%lékař%' THEN 'lékař/doktor'
 WHEN LOWER(cca."StringValue") LIKE 'doktor%' THEN 'lékař/doktor'
 WHEN LOWER(cca."StringValue") LIKE '%lektor%' THEN 'lektor/ka'
 WHEN LOWER(cca."StringValue") LIKE '%školitel%' THEN 'lektor/ka'
 WHEN LOWER(cca."StringValue") LIKE '%logisti%' THEN 'logistik'
 WHEN LOWER(cca."StringValue") LIKE '%malíř%' THEN 'malíř'
 WHEN LOWER(cca."StringValue") LIKE '%manaž%' THEN 'manažer'
 WHEN LOWER(cca."StringValue") LIKE '%manag%' THEN 'manažer'
 WHEN LOWER(cca."StringValue") LIKE '%market%' THEN 'marketing'
 WHEN LOWER(cca."StringValue") LIKE '%masér%' THEN 'masér/ka'
 WHEN LOWER(cca."StringValue") LIKE '%md' THEN 'mateřská' 
 WHEN LOWER(cca."StringValue") LIKE '%mateř%' THEN 'mateřská' 
 WHEN LOWER(cca."StringValue") LIKE '%mat. dov%' THEN 'mateřská'
 WHEN LOWER(cca."StringValue") LIKE '%mat.dov%' THEN 'mateřská'
 WHEN LOWER(cca."StringValue") LIKE '%mechanik%' THEN 'mechanik'
 WHEN LOWER(cca."StringValue") LIKE '%mechaničk%' THEN 'mechanik'
 WHEN LOWER(cca."StringValue") LIKE 'jemný mechanik%' THEN 'mechanik'
 WHEN LOWER(cca."StringValue") LIKE 'jemná mechanič%' THEN 'mechanik'
 WHEN LOWER(cca."StringValue") LIKE '%mistr%' THEN 'mistr'
 WHEN  LOWER(cca."StringValue") LIKE '%montér%' THEN 'montér/montážník'
 WHEN  LOWER(cca."StringValue") LIKE '%montaž%' THEN 'montér/montážník'
 WHEN  LOWER(cca."StringValue") LIKE '%nákup%' THEN 'nákupčí'
 WHEN  LOWER(cca."StringValue") LIKE '%nástroj%' THEN 'strojař/nástrojař'
 WHEN  LOWER(cca."StringValue") LIKE '%stroja%' THEN 'strojař/nástrojař'
 WHEN  LOWER(cca."StringValue") LIKE '%nezamě%' THEN 'nezaměstnaný'
 WHEN  LOWER(cca."StringValue") LIKE '%obchodní%' THEN 'obchodník'
 WHEN  LOWER(cca."StringValue") LIKE '%obch.zás%' THEN 'obchodník'
 WHEN  LOWER(cca."StringValue") LIKE '%obch. zás%' THEN 'obchodník'
 WHEN  LOWER(cca."StringValue") LIKE '%obchod%' THEN 'obchodník'
 WHEN  LOWER(cca."StringValue") LIKE '%obklad%' THEN 'obkladač'
 WHEN  LOWER(cca."StringValue") LIKE '%obklád%' THEN 'obkladač'
 WHEN  LOWER(cca."StringValue") LIKE '%obráběč%' THEN 'obráběč'
 WHEN  LOWER(cca."StringValue") LIKE '%obrábeč%' THEN 'obráběč'
 WHEN  LOWER(cca."StringValue") LIKE '%obsluha čerp%' THEN 'obsluha_čerpací_stanice'
 WHEN  LOWER(cca."StringValue") LIKE 'operátor' THEN 'operátor/ka'
 WHEN  LOWER(cca."StringValue") LIKE 'operátorka' THEN 'operátor/ka'
 WHEN  LOWER(cca."StringValue") LIKE 'operátor výr%' THEN 'operátor/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%operátor call%' THEN 'operátor_call_centra'
 WHEN  LOWER(cca."StringValue") LIKE '%operátor výr%' THEN 'operátor_výroby'
 WHEN  LOWER(cca."StringValue") LIKE '%operátor ve výr%' THEN 'operátor_výroby'
 WHEN  LOWER(cca."StringValue") LIKE '%ostraha%' THEN 'hlídač'
 WHEN  LOWER(cca."StringValue") LIKE 'strážn%' THEN 'hlídač'
 WHEN  LOWER(cca."StringValue") LIKE 'ochranka' THEN 'hlídač'
 WHEN  LOWER(cca."StringValue") LIKE '%osvč%' THEN 'OSVČ/živnostník' 
 WHEN  LOWER(cca."StringValue") LIKE '%živnostník%' THEN 'OSVČ/živnostník' 
 WHEN  LOWER(cca."StringValue") LIKE '%ošetřovatel%' THEN 'ošetřovatel/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%pečovatel%' THEN 'pečovatel/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%pekař%' THEN 'pekař/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%personal%' THEN 'personalista'
 WHEN  LOWER(cca."StringValue") LIKE '%hr spec%' THEN 'personalista'
 WHEN  LOWER(cca."StringValue") LIKE '%podlah%' THEN 'podlahář'
 WHEN  LOWER(cca."StringValue") LIKE 'podnikat%' THEN 'podnikatel'
 WHEN  LOWER(cca."StringValue") LIKE '%majitel%' THEN 'podnikatel'
 WHEN  LOWER(cca."StringValue") LIKE '%pokojsk%' THEN 'pokojská'
 WHEN  LOWER(cca."StringValue") LIKE '%pokrývač%' THEN 'pokrývač'
 WHEN  LOWER(cca."StringValue") LIKE '%policis%' THEN 'policist/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%policie%' THEN 'policist/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%policej%' THEN 'policist/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%pčr%' THEN 'policist/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%strážník%' THEN 'policist/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%kriminal%' THEN 'policist/ka'
 WHEN  LOWER(cca."StringValue") LIKE 'poradce%' THEN 'poradce'
 WHEN  LOWER(cca."StringValue") LIKE 'konzultant%' THEN 'poradce'
 WHEN  LOWER(cca."StringValue") LIKE '%pošt%' THEN 'pošta'
 WHEN  LOWER(cca."StringValue") LIKE '%práv%' THEN 'právník'
 WHEN  LOWER(cca."StringValue") LIKE '%překlad%' THEN 'překladatel'
 WHEN  LOWER(cca."StringValue") LIKE '%prodavač%' THEN 'prodavač/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%pokladní%' THEN 'prodavač/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%prodejce%' THEN 'prodavač/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%produkční%' THEN 'produkční'
 WHEN  LOWER(cca."StringValue") LIKE '%progr%' THEN 'programátor'
 WHEN  LOWER(cca."StringValue") LIKE '%projektant%' THEN 'projektant/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%provozní%' THEN 'provozní'
 WHEN  LOWER(cca."StringValue") LIKE '%psycho%' THEN 'psycholog'
 WHEN  LOWER(cca."StringValue") LIKE '%realit%' THEN 'reality/makléř'
 WHEN  LOWER(cca."StringValue") LIKE 'makléř%' THEN 'reality/makléř'
 WHEN  LOWER(cca."StringValue") LIKE 'real. mak%' THEN 'reality/makléř'
 WHEN  LOWER(cca."StringValue") LIKE 'real.mak%' THEN 'reality/makléř'
 WHEN  LOWER(cca."StringValue") LIKE '%recepč%' THEN 'recepční'
 WHEN  LOWER(cca."StringValue") LIKE '%referent%' THEN 'referent/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%renti%' THEN 'rentiér'
 WHEN  LOWER(cca."StringValue") LIKE 'rd' THEN 'rodičovská_dovolená'
 WHEN  LOWER(cca."StringValue") LIKE 'rodič%' THEN 'rodičovská_dovolená'
 WHEN  LOWER(cca."StringValue") LIKE 'rod.d%' THEN 'rodičovská_dovolená'
 WHEN  LOWER(cca."StringValue") LIKE 'rod. d%' THEN 'rodičovská_dovolená'
 WHEN  LOWER(cca."StringValue") LIKE '%rodičovská%' THEN 'rodičovská_dovolená'
 WHEN  LOWER(cca."StringValue") LIKE '%rozpočt%' THEN 'rozpočtář'
 WHEN  LOWER(cca."StringValue") LIKE '%ředitel%' THEN 'ředitel'
 WHEN  LOWER(cca."StringValue") LIKE '%řezní%' THEN 'řezník'
 WHEN  LOWER(cca."StringValue") LIKE '%řezni%' THEN 'řezník'
 WHEN  LOWER(cca."StringValue") LIKE '%řidi%' THEN 'řidič'
 WHEN  LOWER(cca."StringValue") LIKE '%sádrokart%' THEN 'sádrokartonář'
 WHEN  LOWER(cca."StringValue") LIKE '%sanitář%' THEN 'sanitář/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%servír%' THEN 'servírka'
 WHEN  LOWER(cca."StringValue") LIKE '%seřizovač%' THEN 'seřizovač'
 WHEN  LOWER(cca."StringValue") LIKE '%skladn%' THEN 'skladník'
 WHEN  LOWER(cca."StringValue") LIKE '%skladov%' THEN 'skladník'
 WHEN  LOWER(cca."StringValue") LIKE '%slévač%' THEN 'slévač'
 WHEN  LOWER(cca."StringValue") LIKE '%soc. prac%' THEN 'sociální_pracovník/ce'
 WHEN  LOWER(cca."StringValue") LIKE '%soc.prac%' THEN 'sociální_pracovník/ce'
 WHEN  LOWER(cca."StringValue") LIKE '%sociální prac%' THEN 'sociální_pracovník/ce'
 WHEN  LOWER(cca."StringValue") LIKE '%soudce%' THEN 'soudce'
 WHEN  LOWER(cca."StringValue") LIKE '%soudkyně%' THEN 'soudce'
 WHEN  LOWER(cca."StringValue") LIKE '%soustružník%' THEN 'soustružník'
 WHEN  LOWER(cca."StringValue") LIKE '%sportov%' THEN 'sportovec'
 WHEN  LOWER(cca."StringValue") LIKE '%starost%' THEN 'starosta'
 WHEN  LOWER(cca."StringValue") LIKE '%státní zam%' THEN 'státní_zaměstnanec'
 WHEN  LOWER(cca."StringValue") LIKE '%státní úř%' THEN 'státní_zaměstnanec'
 WHEN  LOWER(cca."StringValue") LIKE '%st.zam%' THEN 'státní_zaměstnanec'
 WHEN  LOWER(cca."StringValue") LIKE '%st. zam%' THEN 'státní_zaměstnanec'
 WHEN  LOWER(cca."StringValue") LIKE '%stat.zam%' THEN 'státní_zaměstnanec'
 WHEN  LOWER(cca."StringValue") LIKE '%stát.zam%' THEN 'státní_zaměstnanec'
 WHEN  LOWER(cca."StringValue") LIKE '%stát. zam%' THEN 'státní_zaměstnanec'
 WHEN  LOWER(cca."StringValue") LIKE '%stavbyved%' THEN 'stavbyvedoucí'
 WHEN  LOWER(cca."StringValue") LIKE '%stavby ved%' THEN 'stavbyvedoucí'
 WHEN  LOWER(cca."StringValue") LIKE '%stavební technik%' THEN 'stavební_technik'
 WHEN  LOWER(cca."StringValue") LIKE '%stavař%' THEN 'stavař'
 WHEN  LOWER(cca."StringValue") LIKE '%strojník%' THEN 'strojník'
 WHEN  LOWER(cca."StringValue") LIKE 'strojník' THEN 'strojník'
 WHEN  LOWER(cca."StringValue") LIKE '%strojvedoucí%' THEN 'strojvedoucí'
 WHEN  LOWER(cca."StringValue") LIKE '%strojvůdce%' THEN 'strojvedoucí'
 WHEN  LOWER(cca."StringValue") LIKE '%studen%' THEN 'student'
 WHEN  LOWER(cca."StringValue") LIKE '%žák%' THEN 'student'
 WHEN  LOWER(cca."StringValue") LIKE '%studující%' THEN 'student'
 WHEN  LOWER(cca."StringValue") LIKE '%svářeč%' THEN 'svářeč'
 WHEN  LOWER(cca."StringValue") LIKE '%svařeč%' THEN 'svářeč'
 WHEN  LOWER(cca."StringValue") LIKE '%šič%' THEN 'šička'
 WHEN  LOWER(cca."StringValue") LIKE '%supervis%' THEN 'supervisor'
 WHEN  LOWER(cca."StringValue") LIKE '%superviz%' THEN 'supervisor'
 WHEN  LOWER(cca."StringValue") LIKE '%švadlena%' THEN 'švadlena'
 WHEN  LOWER(cca."StringValue") LIKE '%taxi%' THEN 'taxikář'
 WHEN  LOWER(cca."StringValue") LIKE '%teamleader%' THEN 'teamleader'
 WHEN  LOWER(cca."StringValue") LIKE '%team leader%' THEN 'teamleader'
 WHEN  LOWER(cca."StringValue") LIKE '%technik%' THEN 'technik'
 WHEN  LOWER(cca."StringValue") LIKE 'servisní technik%' THEN 'technik'
 WHEN  LOWER(cca."StringValue") LIKE 'technický prac%' THEN 'technik'
 WHEN  LOWER(cca."StringValue") LIKE 'servis.technik%' THEN 'technik'
 WHEN  LOWER(cca."StringValue") LIKE 'technič%' THEN 'technik'
 WHEN  LOWER(cca."StringValue") LIKE 'revizní tech%' THEN 'technik'
 WHEN  LOWER(cca."StringValue") LIKE '%technolo%' THEN 'technolog'
 WHEN  LOWER(cca."StringValue") LIKE '%tesař%' THEN 'tesař'
 WHEN  LOWER(cca."StringValue") LIKE '%thp%' THEN 'THP'
 WHEN  LOWER(cca."StringValue") LIKE '%tiskař%' THEN 'tiskař'
 WHEN  LOWER(cca."StringValue") LIKE '%traktorist%' THEN 'traktorista'
 WHEN  LOWER(cca."StringValue") LIKE '%trenér%' THEN 'trenér'
 WHEN  LOWER(cca."StringValue") LIKE '%truhlář%' THEN 'truhlář'
 WHEN  LOWER(cca."StringValue") LIKE '%účet%' THEN 'účetní'
 WHEN  LOWER(cca."StringValue") LIKE '%učit%' THEN 'učitel/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%pedago%' THEN 'učitel/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%údržbář%' THEN 'údržbář'
 WHEN  LOWER(cca."StringValue") LIKE '%uklizeč%' THEN 'uklízeč/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%uklízeč%' THEN 'uklízeč/ka'
 WHEN  LOWER(cca."StringValue") LIKE 'úředn%' THEN 'úředník'
 WHEN  LOWER(cca."StringValue") LIKE '%vedoucí prodej%' THEN 'vedoucí_prodejny'
 WHEN  LOWER(cca."StringValue") LIKE '%vedoucí prov%' THEN 'vedoucí_provozu'
 WHEN  LOWER(cca."StringValue") LIKE '%vedoucí%' THEN 'vedoucí_čehokoliv'
 WHEN  LOWER(cca."StringValue") LIKE '%voja%' THEN 'voják'
 WHEN  LOWER(cca."StringValue") LIKE '%vojá%' THEN 'voják'
 WHEN  LOWER(cca."StringValue") LIKE '%vychovatel%' THEN 'vychovatel/ka'
 WHEN  LOWER(cca."StringValue") LIKE '%zahradn%' THEN 'zahradník'
 WHEN  LOWER(cca."StringValue") LIKE '%zámeč%' THEN 'zámečník'
 WHEN  LOWER(cca."StringValue") LIKE '%sestra%' THEN 'zdravotní_sestra/bratr'
 WHEN  LOWER(cca."StringValue") LIKE '%zdravotní se%' THEN 'zdravotní_sestra/bratr'
 WHEN  LOWER(cca."StringValue") LIKE '%zdravotní brat%' THEN 'zdravotní_sestra/bratr'
 WHEN  LOWER(cca."StringValue") LIKE '%zedník%' THEN 'zedník'
 WHEN  LOWER(cca."StringValue") LIKE '%zedni%' THEN 'zedník'
 WHEN  LOWER(cca."StringValue") LIKE '%zeměděl%' THEN 'zemědělec'
 WHEN  LOWER(cca."StringValue") LIKE '%zubní lék%' THEN 'zubař'
 WHEN  LOWER(cca."StringValue") LIKE '%zubař%' THEN 'zubař'
 WHEN  LOWER(cca."StringValue") LIKE '%stomatolog%' THEN 'zubař'
 WHEN  LOWER(cca."StringValue") LIKE '%v domácnosti%' THEN 'žena_v_domácnosti'
 WHEN  LOWER(cca."StringValue") LIKE '%hospodyň%' THEN 'žena_v_domácnosti'
 WHEN  LOWER(cca."StringValue") LIKE '%žena v dom%' THEN 'žena_v_domácnosti'
 WHEN  LOWER(cca."StringValue") LIKE '%stolař%' THEN 'stolař'
 WHEN  LOWER(cca."StringValue") LIKE '%lodník%' THEN 'lodník'
 WHEN  LOWER(cca."StringValue") LIKE '%výpravčí%' THEN 'výpravčí'
 WHEN  LOWER(cca."StringValue") LIKE '%lesník%' THEN 'lesník/dřevorubec'
 WHEN  LOWER(cca."StringValue") LIKE '%dřevorubec%' THEN 'lesník/dřevorubec'
 WHEN  LOWER(cca."StringValue") LIKE '%veterinář%' THEN 'veterinář'
 WHEN  LOWER(cca."StringValue") LIKE '%zvěrolék%' THEN 'veterinář'
 WHEN  LOWER(cca."StringValue") LIKE '%vrát%' THEN 'vrátný'
 WHEN  LOWER(cca."StringValue") LIKE '%školník%' THEN 'školník'
 WHEN  LOWER(cca."StringValue") LIKE '%školnic%' THEN 'školník'
 WHEN  LOWER(cca."StringValue") LIKE '%daň%' THEN 'daňový poradce'
 WHEN  LOWER(cca."StringValue") LIKE '%bezpe%' THEN 'bezpečnostní_pracovník'
 WHEN  LOWER(cca."StringValue") LIKE '%zubní tech%' THEN 'zubní_technik'
 WHEN  LOWER(cca."StringValue") LIKE '%zvuk%' THEN 'zvukař'
 WHEN  LOWER(cca."StringValue") LIKE '%zootech%' THEN 'zootechnik'
 WHEN  LOWER(cca."StringValue") LIKE '%zlat%' THEN 'zlatník'
 WHEN  LOWER(cca."StringValue") LIKE '%opravá%' THEN 'opravář'
END AS "Job"
FROM
"Customer_Clients" cc
JOIN "Customer_ClientAttributes" cca ON cc."ClientID" = cca."ClientID"
WHERE cca."AttributeID" = '1010' 
--drop table "clientjobs"
;

--insert client job in CASE we dont have info about his job, but we know his university degree - MUDR AND JUDR
INSERT INTO "clientjobs"
SELECT "ClientID", 
CASE WHEN "Degree" = 'MUDR' THEN 'lékař/doktor'
WHEN "Degree" = 'JUDR' THEN 'právník' END AS "Job"
FROM "tmp_degrees"
WHERE "Degree" in ('MUDR','JUDR')
AND "ClientID" NOT IN (SELECT "ClientID" FROM "clientjobs")
;

---------------------Job rank-------------------------
--table with sorted jobs by avg revenue

CREATE TABLE "jobranks" AS 
SELECT sub1."Job", AVG(sub1."Amount") AS "Amount",
CASE WHEN AVG(sub1."Amount")>=50000 THEN '1'
     WHEN AVG(sub1."Amount")BETWEEN 40000 AND 50000 THEN '2'
     WHEN AVG(sub1."Amount")BETWEEN 30000 AND 40000 THEN '3'
     WHEN AVG(sub1."Amount")BETWEEN 20000 AND 30000 THEN '4'
     WHEN AVG(sub1."Amount")<20000 THEN '5' END AS "Rank",
 RANK() OVER (ORDER BY AVG(sub1."Amount") desc) AS "Rank2"
FROM(
SELECT cj."ClientID", cj."Job", sum(acr."Amount") AS "Amount" FROM "clientjobs" cj
JOIN "Customer_Contracts" ccs ON cj."ClientID" = ccs."ClientID"
JOIN "Customer_ContractTrx" cctx ON ccs."ContractID" = cctx."ContractID"
JOIN "Acct_Revenue" acr ON cctx."ContractTrxID" = acr."ContractTrxID"
GROUP BY cj."ClientID", cj."Job")sub1
GROUP BY sub1."Job"
;

CREATE TABLE "clientjobs2" as
SELECT cj."ClientID", 
cj."Job", 
ifnull(jr."Rank",0) AS "JobRank", 
td."Degree", 
ifnull(dd."Rank2",0) AS "DegreeRank"
FROM "clientjobs" cj
JOIN "jobranks" jr ON cj."Job" = jr."Job"
LEFT OUTER JOIN "tmp_degrees" td ON cj."ClientID" = td."ClientID"
LEFT OUTER JOIN "degrees" dd ON td."Degree" = dd."Degree"
;

INSERT INTO "clientjobs2"
SELECT 
td."ClientID",
0 AS "Job",
0 AS "JobRank",
td."Degree",
dd."Rank2" AS "DegreeRank"
FROM "tmp_degrees" td
JOIN "degrees" dd ON td."Degree" = dd."Degree"
WHERE td."ClientID" NOT IN (SELECT "ClientID" FROM "clientjobs2")
;

