--remainders - how much in advance should customer be contacted, based on product or customer/advisor
--there will also be input from advisors - such as concrete day of contact or never contact customer

--remainders general - based on product
--drop table "Remainders_general";
create table "Remainders_general" ( "ProductGroup" varchar(10), "Days" numeric(3,0)) as
select 'LI' as "ProductGroup", 90 as "Days";
insert into "Remainders_general" select 'MT', 120;
insert into "Remainders_general" select 'IR', 90;
insert into "Remainders_general" select 'IO', 90;
insert into "Remainders_general" select 'CI', 60;
insert into "Remainders_general" select 'PI', 60;
insert into "Remainders_general" select 'BS', 90;
insert into "Remainders_general" select 'PE', 90;
insert into "Remainders_general" select 'LO', 120;


--just a temp table to simulate input from advisors

--remainders advisor - based on input from advisor
create table "Remainders_advisor" as
;

--prepare for tickets - contract anniversaries - just anniversary from effective day in this or next year
create table "Anniversaries" as
select ce."ContractEntityID",
ce."DateEffective",
case when month(ce."DateEffective")<month(current_date()) or (month(ce."DateEffective")=month(current_date()) and day(ce."DateEffective")<day(current_date())) then
          date_from_parts(year(current_date())+1,month(ce."DateEffective"),day(ce."DateEffective"))
     else date_from_parts(year(current_date()),month(ce."DateEffective"),day(ce."DateEffective"))
end as "Anniversary",
cpg."ProductGroup"
from "ContractEntity" ce
join "ContractProductGroups" cpg on ce."ContractEntityID" = cpg."ContractEntityID"
where "ContractPhase" in ('1','2')
;

--contact dates - when to contact customer
create table "ContactDates" as
select
a."ContractEntityID",
dateadd(days, -rg."Days", a."Anniversary") as "ContactDate"
from "Anniversaries" a
join "Remainders_general" rg on a."ProductGroup" = rg."ProductGroup"
;

--ticket table will contain all tickets - with info such as advanced remainder and some others
--create table "Tickets" as
select
"ContractEntityID"||datediff(day, to_date('2018-01-01'), current_date) as "ClientTicketID",
"ContractEntityID",
"ContactDate" as "Date"
from "ContactDates"
;


